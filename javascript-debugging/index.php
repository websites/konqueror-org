<?php
  $site_root = "../";
  $page_title = "Javascript Debugging";
  include("konqueror.inc");
  include("header.inc");
?>

<p>Here you can watch a video presenting the JavaScript Debugger included in <strong>Konqueror</strong>.</p>
<p>The main video was created by Tony Young and was later edited by Giannis Konstantinidis during the Google Code-In Contest.</p>
<p></p>

<table border="0" cellspacing="0" cellpadding="4" style="margin:0 auto;">
    <tr>
        <td style="text-align:center;">
            <iframe width="640" height="390" src="http://www.youtube.com/embed/NJQz2sYSj08?rel=0" frameborder="0" allowfullscreen></iframe>
        </td>
    </tr>
</table>

<?php
  include("footer.inc");
?>