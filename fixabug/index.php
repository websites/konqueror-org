<?php
  $site_root = "../";
  $page_title = "Fix a bug";
  include("konqueror.inc");
  include("header.inc");
?>

<ul>
    <li><a href="https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&amp;product=konqueror&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_severity=critical&amp;bug_severity=grave&amp;bug_severity=major&amp;bug_severity=crash">Major Bug reports</a></li>
    <li><a href="https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&amp;product=konqueror&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_severity=normal&amp;bug_severity=minor">Minor Bug reports</a></li>
    <li><a href="https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&amp;product=konqueror&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_severity=wishlist">Wish reports</a></li>
    <li><a href="https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&amp;product=konqueror&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;cmdtype=doit">Junior Jobs</a></li>
</ul>

<?php
  include("footer.inc");
?>
