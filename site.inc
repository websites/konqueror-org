<?php
$site_title = "Konqueror";
$templatepath = "chihuahua/";
$site_logo_left = siteLogo("/media/images/konqueror.png", "64", "64");
$site_external = true;
$name = "konqueror.org Webmaster";
$mail = "webmaster@konqueror.org";

$site_showkdeevdonatebutton = true;

// track using piwik
$piwikSiteID = 8;
$piwikEnabled = true;
?>
