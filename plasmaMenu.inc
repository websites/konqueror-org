<?php
global $plasmaMenu;

$plasmaMenu->iconPath = "/images/icons/";

$plasmaMenu->addMenu("About", "", "green.icon.png", "#acff08");
    $plasmaMenu->addMenuEntry("Home", "/index.php");
    $plasmaMenu->addMenuEntry("Screenshots", "/screenshots/");
    $plasmaMenu->addMenuEntry("Features", false);
        $plasmaMenu->addSubMenuEntry("Browser", "/features/browser.php");
        $plasmaMenu->addSubMenuEntry("File manager", "/features/filemanager.php");
        $plasmaMenu->addSubMenuEntry("Universal viewer", "/features/viewer.php");
        $plasmaMenu->addSubMenuEntry("Customizable application", "/features/application.php");
    $plasmaMenu->addMenuEntry("FAQ", "/faq/");

$plasmaMenu->addMenu("Technical", "", "gray.icon.png", "#aaaaaa");
    $plasmaMenu->addMenuEntry("CSS support", "/css/");
    $plasmaMenu->addMenuEntry("Javascript debugging", "/javascript-debugging/");
    $plasmaMenu->addMenuEntry("Konqueror embedded", "/embedded/");
    $plasmaMenu->addMenuEntry("Embedded tutorial", "/componentstutorial/");
        $plasmaMenu->addSubMenuEntry("Part 1", "/componentstutorial/index.php");
        $plasmaMenu->addSubMenuEntry("Part 2", "/componentstutorial/page2.php");
        $plasmaMenu->addSubMenuEntry("Part 3", "/componentstutorial/page3.php");
        $plasmaMenu->addSubMenuEntry("Part 4", "/componentstutorial/page4.php");
        $plasmaMenu->addSubMenuEntry("Part 5", "/componentstutorial/page5.php");
        $plasmaMenu->addSubMenuEntry("Part 6", "/componentstutorial/page6.php");
        $plasmaMenu->addSubMenuEntry("Part 7", "/componentstutorial/page7.php");

$plasmaMenu->addMenu("Contribute", "", "orange.icon.png", "#ffae00");
    $plasmaMenu->addMenuEntry("Developers", "/developers/");
    $plasmaMenu->addMenuEntry("Investigating bugs", "/investigatebug/");
    $plasmaMenu->addMenuEntry("Fix a bug", "/fixabug/");

$plasmaMenu->addMenu("Download", "/download/", "red.icon.png", "#ff96af");
    $plasmaMenu->addMenuEntry("Download Konqueror", "/download/");
    $plasmaMenu->addMenuEntry("Get the source code", "/getthesource/");

$plasmaMenu->addMenu("Get help", "", "purple.icon.png", "#e285ff");
    $plasmaMenu->addMenuEntry("Konqueror on UserBase", "http://userbase.kde.org/Konqueror");
    $plasmaMenu->addMenuEntry("Community forums", "http://forum.kde.org/viewforum.php?f=66");
    $plasmaMenu->addMenuEntry("Konqueror handbook", "http://docs.kde.org/stable/en/applications/konqueror/index.html");
    $plasmaMenu->addMenuEntry("Contact us", "/contact/");
?>
