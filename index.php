<?php
  $site_root = "./";
  $page_title = "What is Konqueror?";
  include("konqueror.inc");
  include("header.inc");
?>
<style type="text/css">
#main img { background-image:none; border:0 none; border-radius:10px; padding:5px; margin:0; background-color:#fff; }
#main table { border:0 none; }
#main table table {border:1px solid #E1EAF2; border-radius:10px; }
#main table tr,
#main table tr:hover { background:#fff; }
#main table td.description { vertical-align:top; padding:20px; }
</style>
<table border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="pics/konqueror-file-manager.png"><img src="pics/konqueror-file-manager.thumb.png" width="300" height="227" alt="File Manager" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2><a href="features/filemanager.php" title="File Manager">File manager</a></h2>
                        <b>Konqueror</b> is one of the most advanced file managers for KDE. Thanks to the underlying KDE technologies it can transparently access <abbr title="File Transfer Protocol">FTP</abbr> and <abbr title="Secure File Transfer Protocol">SFTP</abbr> servers, zip files (and other archives), smb (Windows) shares, and even browse and rip audio CDs.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2><a href="features/browser.php" title="Web Browser">Web browser</a></h2>
                        <b>Konqueror</b> is powered by the KHTML rendering engine (and optionally Webkit) which means it supports the latest web standards such as HTML5, Javascript, CSS3, and others.
                    </td>
                    <td>
                        <a href="pics/konqueror-web-browser.png"><img src="pics/konqueror-web-browser.thumb.png" width="300" height="200" alt="Web Browser" border="0" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="pics/konqueror-universal-viewer.png"><img src="pics/konqueror-universal-viewer.thumb.png" width="300" height="200" alt="Universal Viewer" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2><a href="features/viewer.php" title="Universal Viewer">Universal viewer</a></h2>
                        <b>Konqueror</b> makes use of the latest KDE technologies to provide you with a PDF viewer, an FTP client, a text editor, a spreadsheet editor, a word document editor, an SVN client and more without ever needing to open a separate application.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2><a href="features/application.php" title="Customizable application">Customizable application</a></h2>
                        <b>Konqueror</b> comes with a nice set of custom plugins such as an ad blocker, automatic web page translation, user agent switcher, automatic image gallery creation, shell command panel, and more. <br />It is one of the most customizable applications available.
                    </td>
                    <td>
                        <a href="pics/konqueror-swiss-army-knife.png"><img src="pics/konqueror-swiss-army-knife.thumb.png" width="300" height="200" alt="Swiss Army Knife" border="0" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?php
// kde_general_news("./news.rdf", 5, true);
?>

<?php
  include("footer.inc");
?>
