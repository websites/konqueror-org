<?php
$site_root = "../";
$page_title = "Screenshots";
include("konqueror.inc");
include("header.inc");
?>
<?php
$gallery = new ImageGallery("Konqueror 4.5.1 running on KDE 4.5.1");
    $gallery->addImage("../pics/konqueror-file-manager.thumb.png", "../pics/konqueror-file-manager.png", "220", "165", "File Manager", "File manager", 0);
    $gallery->addImage("../pics/konqueror-web-browser.thumb.png",  "../pics/konqueror-web-browser.png", "220", "165", "Web Browser", "Web Browser", 0);
    $gallery->addImage("../pics/konqueror-universal-viewer.thumb.png",  "../pics/konqueror-universal-viewer.png", "220", "165", "Universal Viewer", "Universal Viewer", 0);

$gallery->startNewRow();
    $gallery->addImage("../pics/konqueror-file-manager-col-view.thumb.png", "../pics/konqueror-file-manager-col-view.png", "220", "170", "File Manager - Column view", "File manager - Column view", 0);
    $gallery->addImage("../pics/konqueror-filesize-view.thumb.png",  "../pics/konqueror-filesize-view.png", "220", "165", "Filesize view", "Filesize view", 0);
    $gallery->addImage("../pics/konqueror-split-view-and-terminal-emulator.thumb.png",  "../pics/konqueror-split-view-and-terminal-emulator.png", "220", "192", "Split view and terminal emulator", "Split view and terminal emulator", 0);

$gallery->startNewRow();
    $gallery->addImage("../pics/konqueror-svn.thumb.png", "../pics/konqueror-svn.png", "220", "192", "SVN Client", "SVN Client", 0);
    $gallery->addImage("../pics/konqueror-plugins.thumb.png",  "../pics/konqueror-plugins.png", "220", "177", "Plugins", "Plugins", 0);
    $gallery->addImage("../pics/konqueror-settings.thumb.png",  "../pics/konqueror-settings.png", "220", "154", "Settings dialog", "Settings dialog", 0);

$gallery->show();
?>
<?php
include("footer.inc");
?>