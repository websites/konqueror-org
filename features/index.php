<?php
  $site_root = "../";
  $page_title = "Features";
  include("konqueror.inc");
  include("header.inc");
?>

<p>Konqueror makes working with and managing your files easy. You can browse both local and networked folders while enjoying advanced features such as the powerful sidebar and file previews.</p>

<p>Konqueror is also a full featured and easy to use web browser which you can use to explore the Internet.</p>

<p>
In addition to <a href="browser.php">browsing files and web sites</a>, 
Konqueror utilizes KIO plugins
to extend its capabilities well beyond those of other browsers and file
managers. It uses components of KIO, the Konqueror I/O plugin system, to
access different protocols such as HTTP and
FTP (support for these
protocols is built-in).
</p>

<p>
Similarly, Konqueror can use KIO plugins (called IOslaves) to access ZIP
files and other
archives, smb (Windows) shares, to process ed2k links (edonkey/emule), or
even to browse audio CDs,
("audiocd:/") and rip them via
drag-and-drop. 
These capabilities are used when
Konqueror is in <a href="viewer.php">file viewer</a> mode.
The "man:"
and "info:" IOslaves are handy for fetching nicely formatted documentation.
</p>

<p>
Finally, Konqueror can act as a <a href="filemanager.php">file manager</a>
for local files but also, through FTP, WebDAV or other protocols,
as a file manager for files on remote machines.
The FISH ("fish://user@host") protocol can be used to manipulate
files through SSH, allowing you to use Konqueror as the file manager
for almost any machine you can connect to on the internet.
</p>

<?php
  include("footer.inc");
?>
