<?php
  $site_root = "../";
  $page_title = "Customizable Application";
  include("konqueror.inc");
  include("header.inc");
?>

<style type="text/css">
#main img { background-image:none; border:0 none; border-radius:10px; padding:5px; margin:0; background-color:#fff; }
#main table { border:0 none; }
#main table table {border:1px solid #E1EAF2; border-radius:10px; }
#main table tr,
#main table tr:hover { background:#fff; }
#main table td.description { vertical-align:top; padding:20px; }
#main ul li { color:#444; }
</style>

<p><strong>Konqueror</strong> is not only a very responsive and agile filemanager, as well as a web browser/universal document viewer; but it is also a fully customizable application which anybody can configure to suit their own needs. As can be seen from the various screenshots hosted on this site, there are many different ways to configure <strong>Konqueror</strong> - too many to list here. However, just because there are a myriad of configuration possibilities, that's no reason to rule out the basics of what can be changed exactly.</p>
<p></p>

<table border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-file-manager.png"><img src="../pics/konqueror-file-manager.thumb.png" width="300" height="229" alt="Konqueror defauls" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Default view</h2>
                        In this screenshot, you have the standard Konqueror window with the default KDE theme. This is what users will see when KDE is first installed on any system, unless of course the vendor decides to ship with altered defaults.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2>Customized Konqueror</h2>
                        By firing up the configuration window you can configure Konqueror to suit your own needs. Almost anything is a configuration option away!
                    </td>
                    <td>
                        <a href="../pics/konqueror-swiss-army-knife.png"><img src="../pics/konqueror-swiss-army-knife.thumb.png" width="300" height="229" alt="Customized Konqueror" border="0" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-plugins.png"><img src="../pics/konqueror-plugins-2.thumb.png" width="300" height="241" alt="Plugins" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Plugins</h2>
                        A wide array of plugins are provided by default and you can build your own plugin too!
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<p></p>
<p>Since the entire KDE suite of applications is also very configurable, this means that laptop users can have configurations that fit laptops (small icons, low-color-count configurations and so forth); people who really miss Windows (c) can choose to use a Qt theme that looks very similar. They can also use the graphical configuration tool to set up Konqueror's toolbar to look just like Internet Explorer or Firefox.</p>

<?php
  include("footer.inc");
?>
