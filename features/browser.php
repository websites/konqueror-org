<?php
  $site_root = "../";
  $page_title = "Web Browser";
  include("konqueror.inc");
  include("header.inc");
?>

<style type="text/css">
#main img { background-image:none; border:0 none; border-radius:10px; padding:5px; margin:0; background-color:#fff; }
#main table { border:0 none; }
#main table table {border:1px solid #E1EAF2; border-radius:10px; }
#main table tr,
#main table tr:hover { background:#fff; }
#main table td.description { vertical-align:top; padding:20px; }
#main table td.description h3 { padding-top:15px; }
</style>

<p>At the heart of <strong>Konqueror</strong> is the KHTML rendering engine (which was chosen by Apple to create WebKit, which today forms the basis for modern browsers like Safari and Chrome). It currently supports the latest Web Standards such as HTML5, Javascript, CSS3 and others. Alternatively, <strong>Konqueror</strong> can also use Webkit if you're looking for compatibility across the board.</p>

<h2>Some of its features...</h2>

<table border="0" cellspacing="0" cellpadding="4" width="100%">
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="description" style="background:url('../pics/icon-adblock.png') no-repeat 300px 15px;">
                        <h3>Adblock</h3>
                        <p>Automatically block all ads! You can even block other annoying elements from specific websites by manual selection or regular expressions.</p>
                    </td>
                    <td width="50%" class="description" style="background:url('../pics/icon-popup.png') no-repeat 300px 15px;">
                        <h3>Pop-up blocker</h3>
                        <p>Forget about annoying popups, with Konqueror they are a thing of the past!</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="description" style="background:url('../pics/icon-kwallet.png') no-repeat 300px 15px;">
                        <h3>Password Manager</h3>
                        <p>You'll never have to remember or retype a password anymore. Now everytime you visit your favourite site Konqueror will automatically fill in your username & password.</p>
                        <p>It uses KWallet for password storage, that means your passwords are stored securely and shared across all your KDE applications.</p>
                    </td>
                    <td width="50%" class="description" style="background:url('../pics/icon-web-shortcuts.png') no-repeat 300px 15px;">
                        <h3>Web shortcuts</h3>
                        <p>Simply type <i>wp:konqueror</i> or <i>wp:kde</i> in the Address bar to be taken directly to the Wikipedia's page about Konqueror or KDE respectively. There's Web Shortcuts for Google, Youtube, Facebook, Amazon, Yahoo, KDE, Launchpad, etc.</p>
                        <p>Adding your custom Web Shortcut is super easy too!</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="description" style="background:url('../pics/icon-splitview.png') no-repeat 310px 15px;">
                        <h3>Tabbed browsing and split views</h3>
                        <p>Easily preview websites side by side with Konqueror's split view functionality.</p>
                    </td>
                    <td width="50%" class="description" style="background:url('../pics/icon-bookmarks.png') no-repeat 300px 15px;">
                        <h3>Bookmarks Manager</h3>
                        <p>Full featured bookmarks manager for easy access to your most visited sites.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="description" style="background:url('../pics/icon-localize.png') no-repeat 300px 15px;">
                        <h3>Web page translation</h3>
                        <p>You can easily translate any web page into any language you choose from the dropdown menu.</p>
                    </td>
                    <td width="50%" class="description" style="background:url('../pics/icon-spellcheck.png') no-repeat 300px 15px;">
                        <h3>Spell Checking</h3>
                        <p>Integrated spell checking means no more embarassing typos!</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?php
  include("footer.inc");
?>
