<?php
  $site_root = "../";
  $page_title = "Universal Viewer";
  include("konqueror.inc");
  include("header.inc");
?>

<style type="text/css">
#main img { background-image:none; border:0 none; border-radius:10px; padding:5px; margin:0; background-color:#fff; }
#main table { border:0 none; }
#main table table {border:1px solid #E1EAF2; border-radius:10px; }
#main table tr,
#main table tr:hover { background:#fff; }
#main table td.description { vertical-align:top; padding:20px; }
#main ul li { color:#444; }
</style>

<p><strong>Konqueror</strong> can also serve as a viewer for a lot of file types. Click on an image and it will display the image in the current view. Click on a text file, it will show its contents. Same for postscript and DVI files, KOffice files, and others.</p>
<p>On the technical side, the very nice thing about this is that those viewers are not part of <strong>Konqueror</strong> and haven't been developed specifically for it.</p>
<p><strong>Konqueror</strong> embeds components (parts) provided by other applications. The image-viewing part is KView, the text-viewing part is KWrite, the DVI viewer KDVI, the PostScript viewer KGhostview, and of course all KOffice documents are shown by their originating application.</p>
<p>Any new type of file you want to be able to display inside <strong>Konqueror</strong>, you just need to write a part and register it with a desktop file. No need to touch <strong>Konqueror</strong> in any case. Embedding parts from other applications is provided by the library called KParts.</p>
<p></p>

<table border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-viewer-img.png"><img src="../pics/konqueror-viewer-img.thumb.png" width="300" height="229" alt="Image Viewer" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Image Viewer</h2>
                        Preview any type of image (PNG, JPG, GIF, BMP) thanks to the KView component.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2>Word Viewer</h2>
                        Preview and Word document (MS Word or OpenDocument format) as well as the whole Calligra suite including spreadsheets, presentations, project planning, data management, vector graphics, etc.
                    </td>
                    <td>
                        <a href="../pics/konqueror-viewer-word.png"><img src="../pics/konqueror-viewer-word.thumb.png" width="300" height="229" alt="Word Viewer" border="0" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-viewer-presentation.png"><img src="../pics/konqueror-viewer-presentation.thumb.png" width="300" height="229" alt="Presentation Viewer" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Presentation Viewer</h2>
                        Preview any presentation be it in MS Powerpoint or OpenDocument format!
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2>SVN Viewer</h2>
                        Work with SVN repositories right inside Konqueror. Commit revisions, add files, delete files, blame, view history, you name it!
                    </td>
                    <td>
                        <a href="../pics/konqueror-svn.png"><img src="../pics/konqueror-svn-2.thumb.png" width="300" height="262" alt="SVN Viewer" border="0" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-viewer-txt.png"><img src="../pics/konqueror-viewer-txt.thumb.png" width="300" height="229" alt="Text Viewer" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Text Viewer</h2>
                        View and edit text files right inside Konqueror with color-coding, code folding, spell check and most of the features you can find in Kate (KDE's text editor)
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2>And more...</h2>
                        Basically every KDE application supporting KParts can be embedded into Konqueror.
                    </td>
                    <td>
                        <a href="../pics/konqueror-universal-viewer.png"><img src="../pics/konqueror-universal-viewer.thumb.png" width="300" height="229" alt="PDF Viewer" border="0" /></a>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?php
  include("footer.inc");
?>
