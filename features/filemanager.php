<?php
  $site_root = "../";
  $page_title = "File Manager";
  include("konqueror.inc");
  include("header.inc");
?>

<style type="text/css">
#main img { background-image:none; border:0 none; border-radius:10px; padding:5px; margin:0; background-color:#fff; }
#main table { border:0 none; }
#main table table {border:1px solid #E1EAF2; border-radius:10px; }
#main table tr,
#main table tr:hover { background:#fff; }
#main table td.description { vertical-align:top; padding:20px; }
#main ul li { color:#444; }
</style>

<p>At a first glance, <strong>Konqueror</strong> looks just like a normal file manager:</p>

<ul>
    <li>It displays files and directories using the "icons view" (three icon sizes), "columns view" or the "details view" (detailed view in which you can open sub-directories)</li>
    <li>It allows copying, moving and deleting, by direct drag and drop or by using copy, cut and paste.</li>
    <li>It provides properties on a file, to see and change its attributes in a dialog box.</li>
</ul>

<p>But once you start using it you realize some of the more advanced functionality:</p><br />

<table border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-network-transparency.png"><img src="../pics/konqueror-network-transparency.thumb.png" width="300" height="229" alt="Network Transparency" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Network Transparency</h2>
                        Konqueror can transparently access <abbr title="File Transfer Protocol">FTP</abbr> and <abbr title="Secure File Transfer Protocol">SFTP</abbr> servers, zip files (and other archives), smb (Windows) shares, and even browse and rip audio CDs.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="description">
                        <h2>Split views and tabbed browsing</h2>
                        Work on two (or more) different directories side by side, drag & drop, and even preview or edit files right inside Konqueror!
                    </td>
                    <td>
                        <a href="../pics/konqueror-split-views.png"><img src="../pics/konqueror-split-views.thumb.png" width="300" height="229" alt="Split views" border="0" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <a href="../pics/konqueror-terminal.png"><img src="../pics/konqueror-terminal.thumb.png" width="300" height="229" alt="Terminal pane" border="0" /></a>
                    </td>
                    <td class="description">
                        <h2>Terminal pane</h2>
                        Use the terminal pane to embedded a full featured terminal shell inside Konqueror to work on your current view.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<h2>Other features</h2>

<ul>
    <li>Undo functionality</li>
    <li>Support for service menus (easily compress/decompress files, convert image formats, version control integration, etc)</li>
    <li>Automatic updating of directories (no need to manually refresh to view changes)</li>
    <li>Ability to mass rename multiple files</li>
    <li>Filter files by type, etc</li>
</ul>

<?php
  include("footer.inc");
?>
