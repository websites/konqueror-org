<?php
  $site_root = "../";
  $page_title = "Site Launch Announcement";
  include("konqueror.inc");
  include("header.inc");
?>

<p>DATELINE MAY 2, 2000</p>
<p>FOR IMMEDIATE RELEASE</p>
<h2 align="center">Konqueror.org Launches</h2>
<p><strong>New Website Dedicated to Users of KDE's Next-Generation
File Manager/Browser/Universal Viewer</strong></p>
<p>May 2, 2000 (New York, New York).  The <a href="http://www.kde.org">KDE
Team</a> is pleased
to announce the launch of <a href="http://www.konqueror.org">Konqueror.org</a>,
a website devoted to Konqueror, KDE's next-generation, full-featured,
powerful, flexible, modular and Internet-transparent
<a href="http://konqueror.kde.org/konq-filemanager.html">file manager</a>,
<a href="http://konqueror.kde.org/konq-browser.html">web browser</a> and
<a href="http://konqueror.kde.org/konq-viewer.html">universal document
viewer</a>.</p>

<p>Konqueror, which has been widely acclaimed as a technological
break-through for the Linux desktop, has a component-based architecture
which combines the features and functionality of Internet
Explorer<sup>&reg;</sup>/Netscape Communicator<sup>&reg;</sup> and
Windows Explorer<sup>&reg;</sup>.  Konqueror supports
all major Internet technologies, including JavaScript<sup>TM</sup>,
Java<sup>&reg;</sup>, HTML 4.0, CSS-2 (Cascading Style Sheets), SSL
(Secure Socket Layer for secure communications) and, in the near
future, Netscape<sup>&reg;</sup> plugins (for viewing Flash<sup>TM</sup>,
RealAudio<sup>TM</sup>, RealVideo<sup>TM</sup> and similar technologies).
In addition,
Konqueror's network transparency offers seamless support for browsing
Linux<sup>&reg;</sup> NFS shares, Windows<sup>&reg;</sup> SMB shares
and FTP directories.  Konqueror will ship as part of the highly
anticipated release of KDE 2.0, targeted for mid-summer 2000.</p>

<p>Konqueror's developers created
<a href="http://www.konqueror.org">Konqueror.org</a> to serve as
Konqueror's official website.  There
users can learn about Konqueror's many powerful features, keep abreast
of the latest Konqueror news and developments, learn how to download and install
Konqueror and submit comments and questions to Konqueror developers.
Developers can learn how they can use Konqueror to web-enable their
applications, how they can enable their applications to serve as a
Konqueror component and how they can contribute to Konqueror's
ongoing development.</p>

<p>"While the site offers rich content already - including a number of
mouth-watering screenshots - it is a work in progress and we will certainly
continually improve it",
said <a href="mailto:lee@azsites.com">Chris Lee</a>,
webmaster and principal architect of the site.  "We plan to add tutorials
and FAQs for users, include a plug-in and component directory and
supplement the developer's news and tutorials.  This will be <em>the</em>
site to go to for official, as well as unofficial, Konqueror news and
information."</p>

<p>"Konqueror is an extremely promising technology which is poised to
revolutionize Linux not only on the corporate and consumer desktops but
also on networked appliances and personal digital assistants", said
<a href="mailto:pour@mieterra.com">Andreas Pour</a>, President
of MieTerra LLC.  "With the new web site, the Konqueror developers are
again demonstrating their deep commitment to support their users,
assist third-party developers and create the world's premier browser."</p>

<h2>About KDE</h2>
<p>KDE is a collaborative project by hundreds of developers worldwide to
create a sophisticated, customizable and stable desktop environment
employing a network-transparent, intuitive user interface.  Currently
development is focused on KDE 2, which will for the first time offer a
free, open source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<sup>&reg;</sup> and
the Macintosh<sup>&reg;</sup>
while remaining loyal to open standards and empowering developers and users
with open source software.  KDE is working proof of how the open source
software development model can create technologies on par with and superior
to even the most complex commercial software.</p>

<p>For more information about KDE, please visit KDE's <a href="http://www.kde.org/whatiskde/">web site</a>.</p>

<hr />

<table border="0" cellpadding="8" cellspacing="0">
<tr><th colspan="2" align="left">
Press Contacts:
</th></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
United&nbsp;States:
</td><td nowrap="nowrap">
Kurt Granroth<br />
granroth@kde.org<br />
(1) 480 732 1752
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (French and English):
</td><td nowrap="nowrap">
David Faure<br />
faure@kde.org<br />
(44) 1225 471 300
</td></tr>
</table>

<?php
  include("footer.inc");
?>
