<?php
  $site_root = "../";
  $page_title = "Announcements";
  include("konqueror.inc");
  include("header.inc");
?>

<p>Available announcements:</p>
<ul>
<li><a href="sitelaunch.php">Konqueror.org site launch announcement</a></li>
<li><a href="reaktivate.php">Reaktivate released</a></li>
</ul>

<?php
  include("footer.inc");
?>
