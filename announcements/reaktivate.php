<?php
  $site_root = "../";
  $page_title = "Reaktivate Released";
  include("konqueror.inc");
  include("header.inc");
?>

<p>DATELINE JULY 9, 2001</p>
<p>FOR IMMEDIATE RELEASE</p>
<h2 align="center">KDE Web Browser Konqueror Gets Activ(eX)ated</h2>
<p><strong>Konqueror Embraces ActiveX, Plays Shockwave Movies</strong></p>
<p>July 9, 2001 (The INTERNET).
<a href="mailto:wildfox@kde.org">Nikolas Zimmermann</a> and
<a href="mailto:malte@kde.org">Malte Starostik</a> today announced
the availability of reaktivate for
<a href="http://konqueror.kde.org/">Konqueror</a>, KDE's web browser.
Reaktivate enables Konqueror to embed
<a href="http://www.microsoft.com/com/tech/ActiveX.asp">ActiveX</a> controls,
such as the popular
<a href="http://macromedia.com/software/shockwaveplayer/">Shockwave</a>
movies, for which no native Linux/Unix solution exists.  Reaktivate relies
on the
<a href="http://www.winehq.org/">WINE</a> libraries to load and run
ActiveX controls.
</p>
<p>
With this addition, Konqueror now enables KDE users to take optimal advantage
of sophisticated websites that make use of Microsoft Internet Explorer plugins,
Netscape Communicator
<a href="http://konqueror.kde.org/faq.html#nsplugin">plugins</a> for Linux and Java applets,
as well as KDE plugins designed using KDE's
<a href="http://developer.kde.org/documentation/tutorials/kparts/">KParts</a>
technology.
</p>
<p>
According to Malte, the reason he and Nikolas implemented reaktivate
is rather simple: it broadens the spectrum of web sites accessible
to Konqueror, and it was possible.
</p>
<h2>Successes and Limitations</h2>
<p>Theoretically,
Reaktivate can eventually be used to embed any ActiveX control into Konqueror.
Currently, however, not all ActiveX controls are compatible with reaktivate.
In particular, the <a href="http://windowsmedia1.com/mg/home.asp">Microsoft
Windows Media Player</a> cannot be installed using reaktivate (though it is not known if a player which is already installed will work with
reaktivate).  Thus it is likely there exist other ActiveX controls which
will not yet work with reaktivate.
Work is ongoing to increase compatability with other ActiveX controls,
including the
<a href="http://www.apple.com/quicktime/">Apple QuickTime</a> plugin.
</p>
<p>
So far, however, reaktivate has been successfully tested with the
following ActiveX controls:
</p>
<table border="0" cellspacing="6" cellpadding="0">
 <tr><th align="left">Control</th><th align="left">Status</th>
     <th align="left">Test-URL</th><th align="left">Screenshots</th></tr>
 <tr valign="top">
     <td nowrap="nowrap"><a href="http://macromedia.com/software/shockwaveplayer/">Macromedia
Shockwave Flash 5</a></td>
     <td>No known problems.</td>
     <td><a href="http://static.kdenews.org/mirrors/malte.homeip.net/base.html">Click here</a></td>
     <td><a href="http://static.kdenews.org/content/reaktivate/flash1.png">[1]</a>, <a href="http://static.kdenews.org/content/reaktivate/flash2.png">[2]</a>, <a href="http://static.kdenews.org/content/reaktivate/flash3.png">[3]</a>, <a href="http://static.kdenews.org/content/reaktivate/flash4.png">[4]</a>, and <a href="http://static.kdenews.org/content/reaktivate/flash5.png">[5]</a></td>
 </tr>
 <tr valign="top">
     <td nowrap="nowrap">
<a href="http://macromedia.com/software/shockwaveplayer/">Macromedia
Shockwave Player 8</a></td>
     <td>Some files require the use of a native msvcrt.dll instead of the
         one provided by winelib. The post-installation dialog is functional
         but hard to decipher due to drawing problems.  Some movies do not
         display properly (only black stripes and rects are shown)</td>
     <td><a href="http://sdc.shockwave.com/shockwave/download/triggerpages_mmcom/default.html">Click here</a></td>
     <td><a href="http://static.kdenews.org/content/reaktivate/shockwave1.png">[1]</a></td>
 </tr>
 <tr valign="top">
     <td nowrap="nowrap"><a href="http://www.livepicture.com/products/viewers/activex/info.html">LivePics</a></td>
     <td>Clicking the "info" button in the toolbar has no result, everything
         else works fine.</td>
     <td><a href="http://static.kdenews.org/mirrors/malte.homeip.net/livepics.html">Click here</a></td>
     <td><a href="http://static.kdenews.org/content/reaktivate/livepics1.png">[1]</a> and <a href="http://static.kdenews.org/content/reaktivate/livepics2.png">[2]</a></td>
 </tr>
</table>
<h2>Note on Security</h2>
<p>
<strong><em>Install ActiveX controls only from sites that you
trust.</em></strong>
Microsoft's ActiveX technology has often been criticized for weak security.
Those controls are dynamic libraries that are executed exactly like any
other piece of code installed on the user's system. This means they have
full access to the file system, the system registry etc.  As a means to
establish the users' trust in the controls a web site wishes to install,
every ActiveX control is cryptographically signed and carries a certificate
issued by an authority known to the web browser (like
<a href="http://www.verisign.com/">VeriSign</a>).  A control
that has no signature or no certificate or if they are invalid will not be
installed.
</p>
<p>
With reaktivate the situation is similar: the installed controls can call
every WinAPI function provided by the WINE libraries and therefore have
access to WINE's registry and all files visible to the WINE installation.
The current implementation of reaktivate will ask the user for
confirmation to install a new control, but it will not check the embedded
certificate and signature. This is due to technical reasons as well as
limited time.  Therefore we strongly advise to install controls only from
sites that you trust.  To save your files from malicious controls, you might
also consider using this feature only from a seperate user account that
has no access to your main user's files. Reaktivate will not run for the
root account.
</p>
<h2>Installing Reaktivate</h2>
<p>
Source code for reaktivate is freely available under a Free, Open Source
license from the
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/reaktivate">kdenonbeta
module</a> in
<a href="http://webcvs.kde.org/">KDE's CVS repository</a>
and its <a href="http://www.kde.org/cvsupmirrors.html">mirrors</a>.
See the <a href="http://www.kde.org/anoncvs.html">KDE website</a> for
information about how to get a module from CVS.  You only need
the toplevel, admin and reaktivate directories from kdenonbeta.  Before
compiling, get the latest <a href="http://www.winehq.org/download.shtml">CVS
version of WINE</a> (a snapshot will likely not be new enough). Next,
apply all patches from reaktivate/patches-for-wine/ against the WINE
sources and build/install WINE.  Finally, you can build and install
reaktivate.
</p>
<p>
<em>Disclaimer</em>: reaktivate is not in any manner sponsored or endorsed
by, affiliated with, or otherwise related to,
<a href="http://www.microsoft.com/">Microsoft Corporation</a>.
</p>
<p>
Thanks to <a href="mailto:pour at kde.org">Andreas "Dre" Pour</a> and
<a href="mailto:navindra@kde.org">Navindra Umanee</a> for assisting in
drafting this release.
</p>
<hr />

<p>
<em>Trademarks Notices.</em>
KDE, K Desktop Environment and Konqueror are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Microsoft, ActiveX, Microsoft Internet Explorer and Windows Media Player
are registered trademarks or trademarks of Microsoft Corporation.
Shockwave is a trademark or registered trademark of Macromedia, Inc. in
the United States and/or other countries.
Netscape and Netscape Communicator are trademarks or registered trademarks
of Netscape Communications Corporation in the United States and other
countries and JavaScript is a trademark of Netscape Communications Corporation.
Apple and Quicktime are trademarks of Apple Computer, Inc., registered in
the U.S. and other countries.
All other trademarks and copyrights referred to in this announcement are the property
of their respective owners.
</p>

<?php
  include("footer.inc");
?>
