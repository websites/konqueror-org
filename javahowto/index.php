<?php
  $site_root = "../";
  $page_title = "Java HOWTO";
  include("konqueror.inc");
  include("header.inc");
?>

<p>
 This document is a small HOWTO that will explain how to
 configure Konqueror to take advantage of its Java support.  The Java support
 relies on the KDE Java Applet Server( KJAS ), which was begun by
 <a href="mailto:rich@kde.org">Richard Moore</a> and was finished
 and is currently maintained by
 <a href="mailto:wynnw@caldera.com">Wynn Wilkes</a>.

 KJAS uses an external jvm which runs in a different process than Konqueror.
 This has the advantage of developers being able to use Konqueror as a
 deployment platform for the latest java technologies without having to worry
 about plugins.  However, some applets out there were designed for older jvm's.
 Some of these may fail to run correctly with the current jvm's required by KJAS.  A general
 rule is: if your jdk's appletviewer can show the applet, then KJAS should.  Please
 verify that before submitting bug reports.
</p>

<b>Software Requirements</b>
<p>
 KJAS requires a Java 2 compatible jvm to operate correctly.  This new release
 has a built in Security Manager that will not work on the Java 1.1 platform.
 You do not need a complete JDK for KJAS to work- you just need a JRE.
</p>
<p>
 KJAS has been tested against IBM's and Blackdown's 1.3 releases, and Sun's 1.2.2 release.
 Currently, Sun's 1.3 jvm is not supported, but work is going on to fix the problems with
 that version.  Hopefully Sun's 1.3.1 release will work.
 
 It's been found that KJAS seems to work best under Blackdown's jvm.
 Kaffe and IBM's 1.1.8 will probably not work- they are not officially supported.
</p>

<p> 
 Blackdown's JVM can be obtained from <a href="http://www.blackdown.org">http://www.blackdown.org</a>.<br />
 IBM's Java products can be found at <a href="http://www.ibm.com/developer/java/">http://www.ibm.com/developer/java/</a>.<br />
 Sun's Java products can be found at <a href="http://java.sun.com/products/">http://java.sun.com/products/</a>.<br />
</p>

<b>Configuring Konqueror</b>
<p>
 Konqueror will default to using 'java' to start the Applet Server.  If you need to use a
 different jvm, or your system does not have 'java' in its path, you can use the KControl
 Module for Konqueror to configure the correct java to use.  For example, if you install
 IBM's IBMJava2-SDK-1.3-5.0.i386.rpm, the rpm will install to /opt/IBMJava2-13.  From
 a konqueror window, choose the Settings->Configure Konqueror Menu option.  In the
 Konqueror Browser module, choose the Java Tab.  In the edit box for 'Path to java executable',
 type /opt/IbmJava2-13/bin/java.  Konqueror will now use the IBM jvm. Icedtea must be installed.
</p>

<b>Getting HTTPS support for Applets</b>
<p>
 You can get applets to work over https with konqueror using Sun Microsystem's JSSE
 (Java Secure Sockets Extension classes).  These are available at <a href="http://java.sun.com/products/jsse">
 http://java.sun.com/products/jsse</a>.  You will need to download JSSE version 1.0.3 (Sun requires
 a free registration to do so).  You will get a zip file that will contain 3 jar files: jcert.jar, jnet.jar, and jsse.jar.
 Simply copy those to your $KDEDIRS/share/apps/kjava/ directory and KJAS will automatically use them.
 If you don't have write access to your $KDEDIRS directory, then copy the three jar files to your
 $KDEHOME/share/apps/kjava/ directory.  Then copy the $KDEDIRS/share/apps/kjava/kjava.jar to
 $KDEHOME/share/apps/kjava/.  Konqueror will then look here instead of the KDEDIRS directory.
</p>
<p>
 Please Note that you do NOT need to edit your java.security file or do any of the steps outlined
 in the JSSE installation howto.  Simply copy the jar files and KJAS will make use of them.
</p>

<b>It still doesn't work, what should I do?</b>
<p>
It depends on the error....

If applets fail to show up, please enable the Java console to make sure that your java process starts.
If the console does not show up, verify that the path that the Konqueror Browser Control Module has set
for the Java executable leads to an actual java executable.

If applets fail to load and the java console spits out ClassFormatExceptions errors, then there is a problem
with the applet.  Most likely it has not been tested on the latest jvm's.  Check the site with the appletviewer
from your jdk. If the appletviewer shows the applet, then KJAS has a bug- otherwise please talk
to the applet owner and ask them to support Java 2 with their applets.

If you do find an applet that should work, but doesn't, by all means enter a bug report.  Please report
what jvm you're using when you do so.
</p>

<?php
  include("footer.inc");
?>
