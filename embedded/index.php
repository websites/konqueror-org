<?php
  $site_root = "../";
  $page_title = "Konqueror Embedded";
  include("konqueror.inc");
  include("header.inc");
?>

<p>The Konqueror/Embedded project attempts to build up a special version of the
web browsing component of the KDE browser Konqueror (in particular its html
rendering engine khtml and its io subsystem) . Konqueror/Embedded runs on the
Qt/Embedded platform for embedded devices, in an environment without a KDE
installation or a X windowing system, as one static binary, being as small as
possible while still providing all essential features of a web browser, 
including 
</p>
<ul>
<li>HTML4</li>
<li>CSS</li>
<li>JavaScript</li>
<li>Cookies</li>
<li>SSL</li>
<li>Non-blocking IO</li>
<li>Builtin Image Viewer</li>
<li>IPv6 support</li>
<li>Full xbel compatible bookmark support and management</li>
</ul>
<p>
Another key feature is the fact that Konq/E is <em>not</em> a fork of the
browser components of KDE but it is part of the actual build process
to copy over the original sources of the rendering engine, the HTTP client
implementation and various support classes and compile them without 
modification. This is accomplished by providing drop-in replacement classes,
providing source compatibility to the original code, while being optimized
in size and functionality for Konq/E . This has the tremendous advantage of
being able to gain from the latest bugfixes and improvements of the
original browser, for free!
</p>

<h1>Mini FAQ</h1>

<?php

$faq = new FAQ();

$faq->addQuestion("How can I make Konq/E pick up the Qt/Embedded library instead of the normal Qt lib?",
"If you want to compile with Qt/Embedded support, then you have to configure
it with the <code>--enable-qt-embedded</code> configure switch. Also make sure
the <code>QTDIR</code> is set correctly.
");

$faq->addQuestion("I heard there's special support for the Qt Palmtop Environment?",
"There is a --enable-qpe configure switch which will make Konq/E link
against the Qt Palmtop Environment libraries (and use them) . The support
involves inherittance of the <code>QPEApplication</code> base application
class and use of the QPE shared status message feature (re-using the
task-bar for status messages, to avoid wasting screen space with an
application status bar) . Make sure to have <code>QPEDIR</code> set then
using this switch.
");

$faq->addQuestion("Where can I find this Qt Palmtop Environment?",
"
Here's the official website:
<a href=\"http://qpe.sourceforge.net/\">http://qpe.sourceforge.net/</a>
and here's the project page, where you can get the latest development
version:
<a href=\"http://www.sourceforge.net/projects/qpe/\">http://www.sourceforge.net/projects/qpe/</a> .
Note that Konq/E requires at least QPE version 1.3.1.
");

$faq->addQuestion("The startup time of Konq/E is very slow when using Qt/Embedded. Is there
a hidden turbo-boost configure switch to accelerate it to lightspeed?",
"
A lot of time on startup is spend on parsing the available fonts. To optimize
this make sure Qt/E only uses the .qpf font files for fonts and no .bdf or
.ttf fonts . This will decrease startup time dramatically and reduce memory
usage. See the Qt documentation of details about .qpf font files and how to
generate them.

");

$faq->addQuestion("How do things look like after a make install?",
"
Basically an installation consists of three components:
</p>
<ul>
<li>the <code>konq</code> binary, installed into <code>$prefix</code></li>
<li>the <code>konqueror</code> startup script, installed into <code>$prefix
</code> as well. This startup script sets some environment variables to
ensure Konq/E finds its data files and knows where to temporarily store data
on the filesystem. Note that you don't need this startup script if you have
the appropriate environment variables set globally in your system already</li>
<li>the <code>share/</code> subdirectory, installed into
<code>$prefix/share</code> , containing some data files (in particular charset
information and the default stylesheet) . Note that these files are
<em>absolutely</em> required for running Konq/E</li>
</ul>
<p>
");

$faq->addQuestion("Is there support for HTTP proxy servers?",
"
Sure, although this is currently a bit rudimentary, with regard to
configurability. If you want to use a HTTP proxy server make sure to
set the <code>HTTP_PROXY</code> environment variable appropriately. Like for
example <code>HTTP_PROXY=&quot;http://proxy.foo.com:3128/&quot;</code> .
In addition the <code>NO_PROXY_FOR</code> environment variable is supported.
Set it up to a list of servers for which you do not want to use a HTTP proxy
server.
<br />
There is work in progress for a nice configuration dialog for this, combined
with storing these settings in a persistent way, not using environment
variables.
");

$faq->addQuestion("Is there support for caching, to speed up browsing?",
"
There are two caches in Konq/e.
<br />
The memory cache. Primarily used for caching images and stylesheets. The
maximum size is 512 kbyte.
<br />
The disk cache. This is disabled by default, however you can turn it on
and control it using the following environment variables:
</p>
<ul>
<li><code>KIO_HTTP_USECACHE</code> - (set to any value to turn on caching)</li>
<li><code>KIO_HTTP_MAXCACHEAGE</code> - Maximum age of cache in seconds (default 14 Days)</li>
<li><code>KIO_HTTP_MAXCACHESIZE</code> - Maximum cache size in Kilobytes (default 5Mb)</li>
</ul>
<p>
");

$faq->addQuestion("Are there more environment variables konq/e reads?",
"
Yes, here are three:
</p>
<ul>
<li><code>KIO_HTTP_PROXY_CONNECT_TIMEOUT</code> - default 10 seconds</li>
<li><code>KIO_HTTP_CONNECT_TIMEOUT</code> - default 20 seconds</li>
<li><code>KIO_HTTP_RESPONSE_TIMEOUT</code> - default 60 seconds</li>
</ul>
<p>
");

$faq->addQuestion("Can I make the home toolbar button load www.mycompany.com instead of
the KDE website?",
"
Yes, the URL loaded when pressing the home button is configurable.
Just provide a <code>konq-embedrc</code> file in <code>$prefix/share/config</code>
and specify a <code>HomeURL=http://your.site.org/</code> in the
<code>[General]</code> group.
");

$faq->addQuestion("Is there a way to make Konq/E load an initial document on startup, instead
of the blank page?",
"Just put a html document called <code>start.html</code> into
<code>$prefix/share/apps/konq-embed</code>.
");

$faq->addQuestion("I'm having trouble with images in html documents. Or Konq/E crashes for
me when loading JPEG Images",
"
Here's how to get optimal image support and how to get rid of crashes related
to jpegs. For GIF and PNG support you can easily use the builtin Qt support.
For MNG the -system-libmng switch for Qt works fine. For JPEG you don't have
to enable the qt builtin jpeg support. Konq/E comes with its own incremental
jpeg loader. In fact the qt jpeg support won't be used. You need to use an
external jpeg library (well, the one and only popuplar libjpeg which you most
likely already have anyway) . To convince configure to pick up your jpeg
library you might probably need the
<code>--with-extra-libs=/path/to/your/jpeg/lib</code> and
<code>--with-extra-includes=/path/to/your/jpeg/includes</code> configure
switches.
");

$faq->addQuestion("Is it possible to internationalize Konq/E, to make the menus show
for instance German text instead of the default English?",
"
Run <code>make messages</code> in the konq-embed directory. This will create
a konqueror.pot file which you can use with standard GNU i18n tools. Like for
example use <code>msgmerge</code> to merge it with your existing translations.
For translating the actual POT files I strongly recomment using
<a href=\"http://i18n.kde.org/tools/kbabel/\">KBabel</a> , it's certainly one of
the very best translation applications out there.
</p>
<p>
To actually use a POT file with Konq/E you have to convert it
to a Qt message file. This is done easily using the <code>msg2qm</code> utility,
shipped with your Qt distribution, in <code>\$QTDIR/tools/msg2qm</code> . Run
it like:
</p>
<pre>
  \$QTDIR/tools/msg2qm/msg2qm konqueror_de.pot konqueror_de.qm
</pre>
<p>
To make Konq/E use the correct Qt message file, install the .qm file into
<code>\$prefix/share/locale</code> . The filename has to be in the format
<code>konqueror_&lt;languagecode&gt;qm</code> . The choice of the language is done on
the basis of the <code>LANG</code> environment variable.
<br />
So for example if you have a german translation installed as
<code>konqueror_de.qm</code> in
<code>/path/where/you/installed/konqueror/share/locale/konqueror_de.qm</code> ,
then just make sure the <code>LANG</code> environment variable is set to
&quot;de&quot; .  Konq/E will then automatically load the message file and
utilize the translations.
<br />
Last but not least: Translation support works only if Qt is compiled with
translation support!! So double-check your qconfig-local to NOT contain
<code>QT_NO_TRANSLATION</code> .
");

$faq->addQuestion("How do I run Flash plugins with Konqueror/Embedded?",
"
You have to enable the Flash add-on (via --enable-add-ons=kflashpart) which
is based on the flash library written by Olivier Debon. This add-on does not
require X but only implements Flash 3 features and a few of the Flash 4.
<br />
Other efforts are on the way to port the Netscape plugin support to Konq/E
(which will require X).
");

$faq->addQuestion("Is there a mailing list where I can ask question about Konq/E or participate
with development?",
"
Yes! Help is needed and more than appreciated! More information about the
dedicated mailinglist can be found at
<a href=\"http://mail.kde.org/mailman/listinfo/konq-e\">http://mail.kde.org/mailman/listinfo/konq-e</a>
</p>
<p>Please keep in mind that I in particular am working on this in my spare free
time. I'm doing it for the fun of it, I don't get any money. So please bear with
me if I don't answer to emails within 5 minutes.
");

$faq->addQuestion("There are tons of special configure options and the like. I'm getting
confused. How does your configuration look like?",
"
Here's what I use for cross-compiling for the iPAQ:<br/>
Qt:
</p>
<pre>
./configure -gif -qt-libpng -no-jpeg -no-mng -no-thread -no-opengl -release
  -shared -no-g++-exceptions -I/usr/local/arm-linux/include -depths 16
  -qconfig local -no-qvfb -xplatform linux-ipaq-g++
</pre>
<p>
Konq/E:
</p>
<pre>
../xconfigure --disable-debug --enable-static --disable-shared
  --enable-qt-embedded --enable-qpe --with-extra-libs=\$PWD/../lib/
  --with-extra-includes=\$PWD/../include -without-ssl
  --prefix=\$PWD/../install
</pre>
<p>
<code>xconfigure</code> is a configure wrapper script I found on the net.
It's handy when cross-compiling for ARM-Linux, as it sets up all necessary
special configure switches to get things right. I put up a copy of it
<a href=\"http://devel-home.kde.org/~hausmann/xconfigure\">here</a> .
");

$faq->addQuestion("How can I add support for an external application handling the foo: protocol?",
"
Edit $prefix/share/config/konq-embedrc and add a section like this
</p>
<pre>
[External Protocols]
mailto=foomailer
myfooprotocol=specialxzyplayer
</pre>
<p>
(this feature is available as of CVS >= 20010625)
");

$faq->addQuestion("Where can I download Konqueror/Embedded?",
"
<p>
We would suggest to use one of the snapshots available. These  
snapshot are created with well known configuration parameters which  
avoiding some of the usual beginner mistakes.
</p>
<p>
The most recent snapshots are available at: <a href=\"http://www.basyskom.de/index.pl/konqe\">http://www.basyskom.de/index.pl/konqe</a>.
</p>
<p>
Various older snapshots are available at: <a href=\"http://www.cirulla.net/kdenox-snapshots/\">http://www.cirulla.net/kdenox-snapshots/</a>.
</p>
<p>
If you're interesting in bleeding edge stuff then you can also check  
out the source from the KDE CVS Repository , in the kdenox module  
(see <a href=\"http://websvn.kde.org/trunk/kdenox/\">http://websvn.kde.org/trunk/kdenox/</a>). Note that besides this  
module you will also need the kdelibs module for building. Make sure  
your copy of kdelibs resides in the same directory as the kdenox module.
</p>
");

$faq->show();

?>

<?php
  include("footer.inc");
?>
