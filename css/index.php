<?php
  $site_root = "../";
  $page_title = "CSS 2.1 &amp; 3 Support in KHTML 3.4";
  include("konqueror.inc");
  include("header.inc");
?>

 <p>Compiled by <a href="mailto:kde@carewolf.com">Allan Sandfeld</a>.</p>

    <table border="1">
      <tr>
        <th>CSS Property</th>

        <th>Supported?</th>

        <th>Comments</th>
      </tr>

      <tr>
        <td>'background'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'background-attachment'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'background-color'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'background-image'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'background-position'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'background-repeat'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-collapse'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-color'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-spacing'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-style'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-top' 'border-right' 'border-bottom' 'border-left'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-top-color' 'border-right-color' 'border-bottom-color' 'border-left-color'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-top-style' 'border-right-style' 'border-bottom-style' 'border-left-style'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-top-width' 'border-right-width' 'border-bottom-width' 'border-left-width'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'border-width'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'bottom'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'box-sizing'</td>

        <td><span class="sup">yes</span></td>

        <td>CSS 3 UI: only KHTML and Opera, but also supported with -moz- in Gecko </td>
      </tr>

      <tr>
        <td>'caption-side'</td>

        <td><span class="sup">yes</span></td>

        <td>Only top and bottom, left and right was deprecated in CSS 2.1</td>
      </tr>

      <tr>
        <td>'clear'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'clip'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'color'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'content'</td>

        <td><span class="sup">yes</span></td>

        <td>KHTML is one of the only browsers to support generated quotes and counters.</td>
      </tr>

      <tr>
        <td>'counter-increment'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'counter-reset'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'cursor'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'direction'</td>

        <td><span class="sup">yes</span></td>

        <td>
        </td>
      </tr>

      <tr>
        <td>'display'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'empty-cells'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'float'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'font'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'font-family'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'font-size'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'font-style'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'font-variant'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'font-weight'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'height'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'left'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'letter-spacing'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'line-height'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'list-style'</td>

        <td><span class="sup">yes</span></td>

        <td>All CSS 2.1 styles plus box. With -kthml- extension also diamond, upper-greek, persian, urdu and arabic-indic from CSS 3 List</td>
      </tr>

      <tr>
        <td>'list-style-image'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'list-style-position'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'list-style-type'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'margin'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'margin-top' 'margin-right' 'margin-bottom' 'margin-left'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'marker-offset'</td>

        <td><span class="unsup">no</span></td>

        <td>Deprecated in CSS3</td>
      </tr>

      <tr>
        <td>'max-height'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'max-width'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'min-height'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'min-width'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'opacity'</td>

        <td><span class="nosup">no</span></td>

        <td>CSS 3 Color: Implemented by WebCore and Gecko</td>
      </tr>

      <tr>
        <td>'outline'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'outline-color'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'outline-offset'</td>

        <td><span class="nosup">no</span></td>

        <td>CSS 3 UI: Implemented by WebCore</td>
      </tr>
      <tr>
        <td>'outline-style'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'outline-width'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'orphans'</td>

        <td><span class="nosup">no</span></td>

        <td>Paged media.</td>
      </tr>

      <tr>
        <td>'overflow'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'padding'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'page-break-before'</td>

        <td><span class="parsup">partial</span></td>

        <td>Paged media.Only auto and always</td>
      </tr>

      <tr>
        <td>'page-break-after'</td>

        <td><span class="parsup">partial</span></td>

        <td>Paged media. Only auto and always</td>
      </tr>

      <tr>
        <td>'page-break-inside'</td>

        <td><span class="nosup">no</span></td>

        <td>Paged media</td>
      </tr>

      <tr>
        <td>'padding-top' 'padding-right' 'padding-bottom' 'padding-left'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'position'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'quotes'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'right'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'table-layout'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'text-align'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'text-decoration'</td>

        <td><span class="sup">yes</span></td>

        <td>value <span class="unsup">blink</span> not supported, it is optional in css2</td>
      </tr>

      <tr>
        <td>'text-indent'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'text-shadow'</td>

        <td><span class="sup">yes</span></td>

        <td>CSS 3 Text: Only implemented by KHTML and WebCore</td>
      </tr>
      <tr>
        <td>'text-transform'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'top'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'unicode-bidi'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'vertical-align'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'visibility'</td>

        <td><span class="parsup">partial</span></td>

        <td>except value <span class="unsup">collapse</span></td>
      </tr>

      <tr>
        <td>'white-space'</td>

        <td><span class="sup">yes</span></td>

        <td>KHTML is the only browser that supports all CSS 2.1 values such as pre-line and pre-wrap</td>
      </tr>

      <tr>
        <td>'widows'</td>

        <td><span class="nosup">no</span></td>

        <td>Paged media.</td>
      </tr>
      <tr>
        <td>'width'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'word-spacing'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>'z-index'</td>

        <td><span class="sup">yes</span></td>

        <td>&nbsp;</td>
      </tr>
    </table>

<?php
  include("footer.inc");
?>
