<?php
  $site_root = "../";
  $page_title = "Get the source code";
  include("konqueror.inc");
  include("header.inc");
?>

<p>You can <a href="https://projects.kde.org/projects/kde/applications/kde-baseapps/repository/revisions/master/show/konqueror">browse the source code</a> on our online GIT repository.</p>

<p>You can checkout the source code by using:</p>

<p><code>
git clone git://anongit.kde.org/kde-baseapps
</code></p>

<p>You can <a href="https://projects.kde.org/projects/kde/applications/kde-baseapps/activity">watch Konqueror development activity</a> KDE Projects website.</p>

<?php
  include("footer.inc");
?>
