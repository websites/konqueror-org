<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 3";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<h2>Converting aKtion to a Shared Library</h2>

<h2 id="plan">The Plan</h2>
<p>
The first real step to converting an existing application to an
embedded component is to turn it into a shared library.  An important
thing to keep in mind is that this will not change the way you use the
program currently.  There will still be a standalone executable called
<tt>aktion</tt> that you can run.  The only difference at this point
is that the <tt>aktion</tt> executable won't have all of its code
compiled in -- it will load it all dynamically.
</p>
<p>
A second important thing to note is that this does not require
<em>any</em> code changes.  The only changes necessary are are a few
simple lines in the Makefile.  To be specific, the file to modify
isn't <tt>Makefile</tt> -- it is <tt>Makefile.am</tt>.  This is
because aKtion, like all good KDE programs, uses the automake/autoconf
system to automate Makefile creation.  In this system,
<tt>Makefile.in</tt> and <tt>Makefile</tt> are both created based on
<tt>Makefile.am</tt>
</p>

<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#FFCCCC">
<b>Thing To Remember:</b><br />The <em>only</em> file that you need to
change is <tt>Makefile.am</tt>
</td></tr></table>
</td></tr></table>
</center>

<p>
Here are the lines from the Makefile that you will need to change.
</p>

<b id="old_makefile"><u>OLD: aktion-1.99/src/Makefile.am</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#C0C0C0">
<pre>
<b>
# this is the program that gets installed.  it's name is used for all
# of the other Makefile.am variables
bin_PROGRAMS = aktion

aktion_SOURCES = main.cpp aktionConf.cpp capture.cpp  \
                 aktionVm.cpp kxanim.cpp principal.cpp
aktion_LDFLAGS = $(all_libraries) $(KDE_RPATH)
aktion_LDADD   = $(LIB_KFILE) $(LIBVM)
</b>
</pre>
</td></tr></table></td></tr></table>
<p>
Those lines basically say "to create the binary program 'aktion', use
the source files specified with <b>aktion_SOURCES</b>, the link flags
specified with <b>aktion_LDFLAGS</b>, and the libraries specified with
<b>aktion_LDADD</b>"
</p>
<p>
As mentioned earlier, you need to change this so that the Makefile
generates a shared library instead of one standalone executable.
</p>
<p>
Here is the result of the replacement:
</p>

<b id="new_makefile"><u>NEW: aktion-1.99/src/Makefile.am</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#C0C0C0">
<pre>
<b>
# since the "real" aktion is a library, we start constructing the
# stuff for it
lib_LTLIBRARIES = libaktion.la
bin_PROGRAMS    = aktion

libaktion_la_SOURCES = main.cpp aktionConf.cpp capture.cpp  \
                       aktionVm.cpp kxanim.cpp principal.cpp
libaktion_la_LDFLAGS = $(all_libraries) -version-info 1:0:0 -module
libaktion_la_LIBADD  = $(LIB_KFILE) $(LIBVM)

aktion_LDFLAGS = $(KDE_RPATH)
aktion_SOURCES = main.cpp
aktion_LDADD   = libaktion.la
</b>
</pre>
</td></tr></table>
</td></tr></table>


<h2 id="linebyline">Line by Line Analysis</h2>
<p>
What is happening in the above lines?  Here is what each group means:
</p>

<ol>
<li>Specify the outputs

<pre>
<b>
bin_PROGRAMS    = aktion
lib_LTLIBRARIES = libaktion.la
</b>
</pre>
<p>
These two lines detail what are the outputs.  Specifically, they
are an executable (specified with 'bin_PROGRAMS') called aktion and
a shared library (specified with 'lib_LTLIBRARIES') called
libaktion.la.
</p>
<p>
All Makefile variables after these will depend on those names.  For
instance, all variables that pertain to the executable will be in
the form 'aktion_<em>SOMEVARIABLE</em>' and all variables that apply to the
shared library will look like 'libaktion_la_<em>SOMEVARIABLE</em>'
</p>
</li>
<li>Convert former executable variables to shared library variables
<pre>
<b>
libaktion_la_SOURCES = main.cpp aktionConf.cpp capture.cpp  \
                       aktionVm.cpp kxanim.cpp principal.cpp
libaktion_la_LDFLAGS = $(all_libraries) -version-info 1:0:0 -module
libaktion_la_LIBADD  = $(LIB_KFILE) $(LIBVM) -lkparts
</b>
</pre>
<p>
You may notice that there are only three changes to the variables,
here.
</p>
<p>
First:  They are all preceded with libaktion_la_ instead of aktion_.<br />
Reason: Everything is a shared library, now.
</p>
<p>
Second: The LDFLAGS variable has the additional '-version-info
         1:0:0 -module'<br />
Reason: Those are standard flags that are needed to create the
        shared library.  You shouldn't have to change them.
</p>

<p>
Third:  The LDADD variable is now called LIBADD<br />
Reason: Same reason as the first -- this is a shared library now
</p>
</li>

<li>Create a standalone executable
<pre>
<b>
aktion_LDFLAGS = $(KDE_RPATH)
aktion_SOURCES = main.cpp
aktion_LDADD   = libaktion.la
</b>
</pre>

<p>
Since all of the code is now in a shared library, the standalone
executable now needs only a main() function (found in main.cpp in
aKtion) and a pointer to the shared library.
</p>
</li>
</ol>
<p>
That's it.  If you build and install this, you'll notice that it works
exactly the same as before.  The only differences you should see is
that the 'aktion' file is much smaller than before and there is an
additional library in <em>$KDEDIR</em>/lib
</p>

<h2 id="visible">Visible Result</h2>
<p>
<img src="img/step1.png" alt="Step 1" width="198" height="195" />
</p>

<h2 id="practical">Practical Matters</h2>
<p>
If your own application uses a standard automake like the one above,
then converting it to a shared library uses exactly the same steps
(and code).  There is nothing specific to aKtion in this procedure.
</p>

<?php
  include("footer.inc");
?>
