<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 7";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<H2>Final Thoughts</H2>

<H2><A NAME="recap">The Steps to a Component: Recap</A></H2>
You may have noticed that converting an application to an embedded
component is pretty easy.  In fact, this tutorial could have easily
condensed the four steps into one or two with no loss of clarity.
That said, you will likely do your conversion in the four stages
outlined, so maybe that wasn't such a bad idea.
<P>
To recap, the four stages are:
<OL>
<LI><B>Convert your application to a shared library</B><P>

    This requires only a change to the Makefile.am file
    <P>
<LI><B>Add the skeleton KParts code</B><P>

    This requires two files to be added to the project, and two minor
    changes to the Makefile.am and your application's .desktop file
    <P>
<LI><B>Add in your own code</B><P>

    This is probably the hardest step.. where you integrate your
    existing code into the KParts interface.
    <P>
<LI><B>Connect GUI elements to control your code</B><P>

    This is where you insert items into the toolbar and edit menu to
    allow the user to control your component.
    <P>
</OL>


<H2><A NAME="unique">What is Boilerplate? What is App-Specific?</A></H2>
Whenever you do a tutorial, the question is what of the tutorial can
you use in your own and what is specific to the app itself.  Well,
you're in luck for this tutorial -- nearly all of the code in this
tutorial was boilerplate, cut-and-paste type code!
<P>
In general, you can copy over the files and just change all references
to 'aktion' and 'Aktion' to your own app naming convention.
<P>
The only thing that is totally app specific is in the AktionPart
class.  Wherever you see references to the <tt>widget</tt> object,
that's where you need to insert your app specific things.
<P>


<H2><A NAME="gotcha">"Gotcha"s.  What to Look Out For</A></H2>
Most of the things to look out for were mentioned in the steps of the
tutorial.  Two things that stand out, though, are these:

<OL>
<LI><B>Pay attention to the .desktop file</B><P>

    If you wonder why Konqueror isn't embedding your app, it's almost
    surely because something is wrong with your .desktop file.  Pay
    close attention to that section in the tutorial.
    <P>
<LI><B>Make sure your app has a pre-existing "view" class</B><P>

     This is probably the most important point in this tutorial.  If
     you app already has a existing view class that provides a
     coherent interface to your apps UI functionality, then everything
     is trivial.  If your code is ad-hoc, then this could be a
     nightmare.
</OL>


<H2><A NAME="links">Links</A></H2>
Here are some links that you may find useful:<P>

<B>Original location of this tutorial:</B><BR>
<A HREF="http://developer.kde.org/documentation/tutorials/components/index.html">
http://developer.kde.org/documentation/tutorials/components/index.html</A><P>

<B>Tutorial pages packaged up (no source)</B><BR>
<A HREF="http://developer.kde.org/documentation/tutorials/components/components-tutorial.tar.bz2">
http://developer.kde.org/documentation/tutorials/components/components-tutorial.tar.bz2</A><P>

<B>Source package for the base aKtion 1.99:</B><BR>
<A HREF="http://developer.kde.org/documentation/tutorials/components/aktion-1.99-base.tar.gz">
http://developer.kde.org/documentation/tutorials/components/aktion-1.99-base.tar.gz</A><P>

<B>Source package for the finished (component) aKtion 1.99:</B><BR>
<A HREF="http://developer.kde.org/documentation/tutorials/components/aktion-1.99-final.tar.gz">
http://developer.kde.org/documentation/tutorials/components/aktion-1.99-final.tar.gz</A><P>

<B>Official aKtion! homepage</B><BR>
<A HREF="http://www.geocities.com/SiliconValley/Haven/3864/aktion.html">
http://www.geocities.com/SiliconValley/Haven/3864/aktion.html</A><P>

<B>Sample AVI file for testing (needs to be unzipped)</B><BR>
<A HREF="http://cmg.simplenet.com/avi_clips/pepe_pen.zip">
http://cmg.simplenet.com/avi_clips/pepe_pen.zip</A><P>

<B>Plugins needed for xanim/aktion to play sample AVI file (Intel
Indeo 4.1)</B><BR>
<A HREF="http://xanim.va.pubnix.com/xa_dlls.html">
http://xanim.va.pubnix.com/xa_dlls.html</A><P>


<?php
  include("footer.inc");
?>
