<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 6";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<H2>Finishing Off With KAction</H2>

<H2><A NAME="plan">The Plan</A></H2>
The aKtion embedded component needs only one thing, now -- it's Play,
Stop, Forward, and Backward buttons displayed on Konqueror's toolbar
and View menu.
<P>
This is done by creating "actions" corresponding to those, well,
actions.  The resulting actions (encapsulated in the KAction class)
can then be "plugged" or unplugged into toolbar or menubars or pretty
much anywhere else.  The location of these actions will be described
in an XML file.
<P>
As with everything else, this takes only a few lines of code.

Here is the aktion_part.h file with the necessary changes in bold:
<P>
&nbsp;<B><U>aktion_part.h</U></B>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR><TD BGCOLOR="#000000">
<TABLE BORDER=0 ALIGN="center" CELLSPACING=2 CELLPADDING="0" WIDTH="100%">
<TR><TD BGCOLOR="#C0C0C0">
<PRE>
#ifndef __aktion_part_h__
#define __aktion_part_h__

#include &lt;kparts/browserextension.h&gt;
#include &lt;klibloader.h&gt;

class KInstance;
class KXAnim;
class AktionBrowserExtension;
<B>class KAction;</B>

class AktionFactory : public KLibFactory
{
    Q_OBJECT
public:
    AktionFactory();
    virtual ~AktionFactory();

    virtual QObject* create(QObject* parent = 0, const char* name = 0,
                            const char* classname = "QObject",
                            const QStringList &amp;args = QStringList());

    static KInstance *instance();

private:
    static KInstance *s_instance;
};

class AktionPart: public KParts::ReadOnlyPart
{
    Q_OBJECT
public:
    AktionPart(QWidget *parent, const char *name);
    virtual ~AktionPart();

    virtual bool closeURL();

protected:
    virtual bool openFile();

protected slots:
    void slotPlay();
    void slotStop();
<B>    void slotForward();
    void slotBackward();</B>

private:
    KXAnim *widget;
    AktionBrowserExtension *m_extension;

<B>    KAction *m_playAction;
    KAction *m_stopAction;
    KAction *m_forwardAction;
    KAction *m_backwardAction;</B>
};

class AktionBrowserExtension : public KParts::BrowserExtension
{
    Q_OBJECT
    friend class AktionPart;
public:
    AktionBrowserExtension(AktionPart *parent);
    virtual ~AktionBrowserExtension();
};

#endif
</PRE>
</TD></TR></TABLE><TD></TR></TABLE>

<P>&nbsp;
<CENTER>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR><TD BGCOLOR="#000000">
<TABLE BORDER=0 ALIGN="center" CELLSPACING=2 CELLPADDING="0" WIDTH="100%">
<TR><TD BGCOLOR="#FFCCCC">
<B>Thing To Remember:</B><BR>
Each <em>real</em> action ("play", "cut", "quit") in your application
should have exactly one corresponding KAction
</TD></TR></TABLE>
</TD></TR></TABLE>
</CENTER>

<P>
Here is the aktion_part.cpp file with changes in bold:<BR>

&nbsp;<B><U>aktion_part.cpp</U></B>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR><TD BGCOLOR="#000000">
<TABLE BORDER=0 ALIGN="center" CELLSPACING=2 CELLPADDING="0" WIDTH="100%">
<TR><TD BGCOLOR="#C0C0C0">
<PRE>
#include "aktion_part.h"

#include &lt;kinstance.h&gt;
#include &lt;klocale.h&gt;
#include &lt;kaboutdata.h&gt;
<B>#include &lt;kaction.h&gt;</B>

#include "kxanim.h"
#include &lt;qtimer.h&gt;

extern "C"
{
    /**
     * This function is the 'main' function of this part.  It takes
     * the form 'void *init_lib&lt;library name&gt;()'.  It always returns a
     * new factory object
     */
    void *init_libaktion()
    {
        return new AktionFactory;
    }
};

/**
 * We need one static instance of the factory for our C 'main'
 * function
 */
KInstance *AktionFactory::s_instance = 0L;

AktionFactory::AktionFactory()
{
}

AktionFactory::~AktionFactory()
{
    if (s_instance)
        delete s_instance;

    s_instance = 0;
}

QObject *AktionFactory::create(QObject *parent, const char *name, const char*,
                               const QStringList&amp; )
{
    QObject *obj = new AktionPart((QWidget*)parent, name);
    emit objectCreated(obj);
    return obj;
}

KInstance *AktionFactory::instance()
{
    if ( !s_instance )
    {
        KAboutData about("aktion", I18N_NOOP("aKtion"), "1.99");
        s_instance = new KInstance(&amp;about);
    }
    return s_instance;
}

AktionPart::AktionPart(QWidget *parent, const char *name)
    : KParts::ReadOnlyPart(parent, name)
{
    setInstance(AktionFactory::instance());

    // create a canvas to insert our widget
    QWidget *canvas = new QWidget(parent);
    canvas-&gt;setFocusPolicy(QWidget::ClickFocus);
    setWidget(canvas);

    m_extension = new AktionBrowserExtension(this);

    // create our animation widget
    widget = new KXAnim(this);
    widget-&gt;setLoop(true);
    widget-&gt;show();

<B>    // create and connect our actions
    m_playAction = new KAction(i18n("Play"), QIconSet(BarIcon("tocar",
                               AktionFactory::instance())), 0, this,
                               SLOT(slotPlay()), actionCollection(),
                               "play");

    m_stopAction = new KAction(i18n("Stop"), QIconSet(BarIcon("parar",
                               AktionFactory::instance())), 0, this,
                               SLOT(slotStop()), actionCollection(),
                               "stop");

    m_backwardAction = new KAction(i18n("Backward"),
                                   QIconSet(BarIcon("retroceder",
                                   AktionFactory::instance())), 0, this,
                                   SLOT(slotBackward()), actionCollection(),
                                   "backward");

    m_forwardAction = new KAction(i18n("Forward"), QIconSet(BarIcon("avanzar",
                                  AktionFactory::instance())), 0, this,
                                  SLOT(slotForward()), actionCollection(),
                                  "forward");

    setXMLFile("aktion_part.rc");</B>
}

AktionPart::~AktionPart()
{
    slotStop();
}

bool AktionPart::openFile()
{
    widget-&gt;setFile(m_file);
    widget-&gt;stop();
    widget-&gt;show();
    QTimer::singleShot(2000, this, SLOT(slotPlay()));

    return true;
}

bool AktionPart::closeURL()
{
    slotStop();
    return true;
}

void AktionPart::slotPlay()
{
    widget-&gt;play();
<B>    m_playAction-&gt;setEnabled(false);
    m_stopAction-&gt;setEnabled(true);
    m_forwardAction-&gt;setEnabled(true);
    m_backwardAction-&gt;setEnabled(true);</B>
}

void AktionPart::slotStop()
{
    widget-&gt;stop();
<B>    m_playAction-&gt;setEnabled(true);
    m_stopAction-&gt;setEnabled(false);
    m_forwardAction-&gt;setEnabled(false);
    m_backwardAction-&gt;setEnabled(false);</B>
}

<B>void AktionPart::slotForward()
{
    widget-&gt;stepForward();
}

void AktionPart::slotBackward()
{
    widget-&gt;stepBack();
}</B>

AktionBrowserExtension::AktionBrowserExtension(AktionPart *parent)
    : KParts::BrowserExtension(parent, "AktionBrowserExtension")
{
}

AktionBrowserExtension::~AktionBrowserExtension()
{
}
</PRE>
</TD></TR></TABLE><TD></TR></TABLE>

<P>

<H2><A NAME="linebyline">Line By Line</A></H2>
Using actions always follow the same steps:
<OL>
<LI>Create KAction with name, icon(s), keyboard shortcut, and slot
<LI>Connect the action with any toolbars/menubars/etc that you want it
to display on using an XML file
</OL>

<CENTER>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR><TD BGCOLOR="#000000">
<TABLE BORDER=0 ALIGN="center" CELLSPACING=2 CELLPADDING="0" WIDTH="100%">
<TR><TD BGCOLOR="#FFCCCC">
<B>Thing To Remember:</B><BR>
There are only two necessary steps to use actions with KParts<P>
1. Create a KAction (or derived class) for each real action<BR>
2. Connect the KAction to the toolbar/menubars in an XML file
</TD></TR></TABLE>
</TD></TR></TABLE>
</CENTER>

The first step is done with this line:
<PRE>
<B>
m_playAction = new KAction(i18n("Play"), QIconSet(BarIcon("tocar",
                           AktionFactory::instance())), 0, this,
                           SLOT(slotPlay()), actionCollection(),
                           "play");
</B>
</PRE>
This creates a new action with the following parameters:<P>

Text to display - "Play"<BR>
Icon to display - tocar.png<BR>
Keyboard accel - none<BR>
SLOT parent - current widget<BR>
SLOT - slotPlay()<BR>
Action parent - the KParts action collection<BR>
action name - used to match XML description with action
<P>
As a side note, KDE has a number of standard actions available for
use.  They take advantage of the fact that certain actions (like
"quit", "open document", etc) have standard text, icons, and keyboard
accelerators.  These parameters are encapsulated into KStdAction
actions.  You, as a developer using them, need only supply a SLOT to
connect to.  aKtion does not use any standard actions, hence they are
not used here.
<P>
The second step (connecting the action to our widgets) is accomplished
with in this file:

<P>
&nbsp;<B><U>aktion_part.rc</U></B>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR><TD BGCOLOR="#000000">
<TABLE BORDER=0 ALIGN="center" CELLSPACING=2 CELLPADDING="0" WIDTH="100%">
<TR><TD BGCOLOR="#C0C0C0">
<PRE>
&lt;!DOCTYPE kpartgui&gt;
&lt;kpartgui name="aktion"&gt;
&lt;MenuBar&gt;
 &lt;Menu name="view"&gt;
  &lt;Action name="play"/&gt;
  &lt;Action name="stop"/&gt;
  &lt;Action name="backward"/&gt;
  &lt;Action name="forward"/&gt;
 &lt;/Menu&gt;
&lt;/MenuBar&gt;
&lt;ToolBar name="Aktion-ToolBar"&gt;
  &lt;Action name="play"/&gt;
  &lt;Action name="stop"/&gt;
  &lt;Action name="backward"/&gt;
  &lt;Action name="forward"/&gt;
&lt;/ToolBar&gt;
&lt;StatusBar/&gt;
&lt;/kpartgui&gt;
</PRE>
</TD></TR></TABLE><TD></TR></TABLE>

<P>
You can see that each action has a corresponding Action item in the
XML.  The names for the Menu and ToolBars are either specific or
generic depending on what you want them to do.  If you use a reserved
name like "view" or "edit", then your actions will be plugged into an
existing menu or toolbar.  If you use a new name like
"Aktion-ToolBar", then you will get a new toolbar.
<P>
You'll also need to add the following lines to your Makefile.am:
<PRE><B>
# this is where the menu and toolbar description file goes
partdir   = $(kde_datadir)/aktion
part_DATA = aktion_part.rc
</B></PRE>
<P>
That's pretty much it!

<P>

<H2><A NAME="visible">Visible Result</A></H2>
<IMG SRC="img/step4.png" ALT="Final Result" WIDTH="353" HEIGHT="276">

<P>

<H2><A NAME="practical">Practical Matters</A></H2>
As you saw, this code was pretty straight-forward.  There really
wasn't anything specific to aKtion, here.  Do check the KStdAction
actions before creating your own, however.  Using those actions are
<em>strongly</em> encouraged.

<?php
  include("footer.inc");
?>
