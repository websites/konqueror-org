<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 2";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<p>Note: This is very old (KDE 1/KDE 2 times) and thus information and links might not be correct anymore.</p>

<h2>Introducing aKtion!</h2>

<h2><a name="plan">Introduction</a></h2>
<p>
If you have used konqueror at all, you have no doubt noticed its
ability to "embed" components for viewing certain types of files.  The
two best known examples are viewing images using the embedded kview
and viewing postscript or PDF files using kghostview.
</p>
<p>
The technology behind the embedding is pretty impressive... but the
authors took great pains to make the API as easy to use as possible.
In fact, this tutorial will show you just how incredibly easy it is to
convert an existing application into an embedded component.  It will
take exactly four tiny steps!
</p>
<p>
The four steps are as follows:
</p>
<ol>
<li>Convert application to shared library (simple change in Makefile)</li>
<li>Add boilerplate KParts component (add one standard file)</li>
<li>Connect existing app to KParts (a few lines of code)</li>
<li>Add toolbar and menu entries (a few more lines of code)</li>
</ol>

<p>
It took this author exactly 15 minutes to convert the example
application to an embedded component.. and it took that long only
because of some quirks in the existing app.  The conversion to
KParts was trivial.
</p>

<h2><a name="aktion">Introducing aKtion!</a></h2>
<p>
The application that this tutorial will use as it's example is
'aKtion!'  aKtion is a KDE video player based on xanim.  It can play
most common forms of animation (AVI, Quicktime, MPEG, etc).  With the
conclusion of this tutorial, it will be able to play those animations
inside of Konqueror!
</p>
<p>
There is one small catch to this: you must start with the version of
aKtion supplied with this tutorial.  There are two "official" versions
of aKtion.  One is still based on KDE 1.x and is not suitable for
KParts.  The other is in kdemultimedia and is already converted to
KParts!  So, if you want to follow along, use
<a href="aktion-1.99-base.tar.gz">this</a> version.
</p>

<h2><a name="started">Getting Started</a></h2>
<p>
The first step is to get the aKtion source package compiled and
installed just to test what it currently does.  This assumes that you
have a working KDE2 development system.  If you do not, read this
first!
</p>

<p>
Follow these steps:
</p>

<ol>
<li>Unpack the aKtion source package<br />
<pre>
% gzip -dc aktion-1.99.tar.gz | tar xvpf -
</pre>
</li>
<li>Build and install it<br />
<pre>
% cd aktion-1.99 &amp;&amp; ./configure &amp;&amp; make &amp;&amp; make install
</pre>
</li>
</ol>

<p>
Now find an animation file somewhere -- either on your local hard
drive or on the web and click on it.  You should see that Konqueror
will download the file (if it is remote) and pass it to aKtion to
play.  Pretty standard, huh?
</p>

<?php
  include("footer.inc");
?>
