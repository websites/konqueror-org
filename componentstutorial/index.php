<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 1";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<p>By <a href="mailto:granroth@kde.org">Kurt Granroth</a></p>

<p>
This tutorial will demonstrate how to convert an existing KDE application (the aKtion! video player) into a component suitable for embedding inside of the Konqueror browser. This is accomplished in four simple steps with copious code listings and explanations.
</p>

<h2>Preface: Tutorial Conventions</h2>

<h2><a name="copyright">Copyright</a></h2>
<p>
This tutorial is Copyright (C) 2000 Kurt Granroth
&lt;<a href="mailto:granroth@kde.org">granroth@kde.org</a>&gt;.
All rights reserved.
</p>

<h2><a name="history">Version History</a></h2>
<p>
1.0 (Jan 03, 2000) - Initial Version<br />
1.1 (Jan 25, 2000) - Converted to KParts<br />
1.2 (Mar 03, 2000) - Change to new KParts::BrowserExtension
</p>

<h2><a name="sections">Sections</a></h2>
<p>
Most steps in the tutorial will have at least four sections:<br />
<b>The Plan</b><br />
This section tells you what you will be doing in this step
</p>

<p>
<b>Line By Line</b><br />
This section will go into the important lines of code in some detail.
It will address only those lines of code that need explanation.</p>

<p>
<b>Visible Result</b><br />
This section contains a screenshot of the example app as it should
appear after the current step</p>

<p>
<b>Practical Matters</b><br />
If you are reading this tutorial, it is likely because you want to use
this knowledge in your own application.  Tutorials are necessarily
very dependent on one example application, however.  This section
tries to bring to light any practical matters that you should keep in
mind when transferring the procedure used for the example app to to
your own app.</p>


<h2><a name="remember">Things to Remember</a></h2>
<p>
Every now and then, a point will come up that the author thinks is
very important to remember.  This information will often be repeated
in a small box like so:
</p>

<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#FFCCCC">
<b>Thing To Remember:</b><br />
Important points will often appear in boxes like this
</td></tr></table>
</td></tr></table>
</center>


<h2><a name="listing">Code Listing</a></h2>
<p>
Most sections will have quite a bit of code listed.  Individual lines
will usually be listed like so:
</p>

<pre><b>
some_code = new IndividualLine(of_code);
</b></pre>

<p>
Listings that exceed a few lines of code (entire files, for instance)
will be displayed in a box as shown below.  The lines that the author
feels are important will be highlighted in bold.
</p>

<p>
&nbsp;<b><u>Listing Description</u></b>
</p>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#C0C0C0">
<pre>
class ExampleClass: public ParentClass
{
    Q_OBJECT
public:
    ExampleClass(QWidget *parent, const char *name);
    virtual ~AktionKonqView();

<b>    virtual void someImportantMethod();</b>

private:
    QWidget *widget;

<b>    NewWidget *m_newWidget;</b>
};
</pre>
</td></tr></table></td></tr></table>

<?php
  include("footer.inc");
?>
