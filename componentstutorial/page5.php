<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 5";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<h2>Making the Component Functional</h2>

<h2 id="plan">The Plan</h2>
<p>
You're on the home stretch now.  You have a fully working embedded
component -- you just need to make it do something!
</p>
<p>
This step is one of the shortest ones in this tutorial but will likely
be the hardest in real use.  This is where you need to connect your
applications functionality to the KParts interface.
</p>
<p>
In the case of aKtion, nearly all of the actual "playing" code is
encapsulated in the KXAnim class.  This made it very easy to use in a
KParts interface.  All that was needed was to replace the QLabel
example with KXAnim and everything worked (mostly).
</p>


<h2 id="files">The Files</h2>
&nbsp;<b><u>aktion_part.h</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#c0c0c0">
<pre>
#ifndef __aktion_part_h__
#define __aktion_part_h__

#include &lt;kparts/browserextension.h&gt;
#include &lt;klibloader.h&gt;

class KInstance;
<b>class KXAnim;</b>
class AktionBrowserExtension;

class AktionFactory : public KLibFactory
{
    Q_OBJECT
public:
    AktionFactory();
    virtual ~AktionFactory();

    virtual QObject* create(QObject* parent = 0, const char* name = 0,
                            const char* classname = "QObject",
                            const QStringList &amp;args = QStringList());

    static KInstance *instance();

private:
    static KInstance *s_instance;
};

class AktionPart: public KParts::ReadOnlyPart
{
    Q_OBJECT
public:
    AktionPart(QWidget *parent, const char *name);
    virtual ~AktionPart();

    virtual bool closeURL();

protected:
    virtual bool openFile();

<b>protected slots:
    void slotPlay();
    void slotStop();</b>

private:
<b>    KXAnim *widget;</b>
};

#endif
</pre>
</td></tr></table><td></tr></table>

&nbsp;<b><u>aktion_part.cpp</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#c0c0c0">
<pre>
#include "aktion_part.h"

#include &lt;kinstance.h&gt;
#include &lt;klocale.h&gt;
#include &lt;kaboutdata.h&gt;

<b>#include "kxanim.h"
#include &lt;qtimer.h&gt;</b>

extern "C"
{
    /**
     * This function is the 'main' function of this part.  It takes
     * the form 'void *init_lib&lt;library name&gt;()'.  It always returns a
     * new factory object
     */
    void *init_libaktion()
    {
        return new AktionFactory;
    }
};

/**
 * We need one static instance of the factory for our C 'main'
 * function
 */
KInstance *AktionFactory::s_instance = 0L;

AktionFactory::AktionFactory()
{
}

AktionFactory::~AktionFactory()
{
    if (s_instance)
        delete s_instance;

    s_instance = 0;
}

QObject *AktionFactory::create(QObject *parent, const char *name, const char*,
                               const QStringList&amp; )
{
    QObject *obj = new AktionPart((QWidget*)parent, name);
    emit objectCreated(obj);
    return obj;
}

KInstance *AktionFactory::instance()
{
    if ( !s_instance )
    {
        KAboutData about("aktion", I18N_NOOP("aKtion"), "1.99");
        s_instance = new KInstance(&amp;about);
    }
    return s_instance;
}

AktionPart::AktionPart(QWidget *parent, const char *name)
    : KParts::ReadOnlyPart(parent, name)
{
    setInstance(AktionFactory::instance());

    // create a canvas to insert our widget
    QWidget *canvas = new QWidget(parent);
    canvas-&gt;setFocusPolicy(QWidget::ClickFocus);
    setWidget(canvas);

<b>    // create our animation widget
    widget = new KXAnim(canvas);
    widget-&gt;setLoop(true);
    widget-&gt;show();</b>
}

AktionPart::~AktionPart()
{
    slotStop();
}

bool AktionPart::openFile()
{
<b>    widget-&gt;setFile(m_file);
    widget-&gt;stop();
    widget-&gt;show();
    QTimer::singleShot(2000, this, SLOT(slotPlay()));</b>

    return true;
}

bool AktionPart::closeURL()
{
<b>    slotStop();</b>
    return true;
}

<b>void AktionPart::slotPlay()
{
    widget-&gt;play();
}

void AktionPart::slotStop()
{
    widget-&gt;stop();
}</b>
</pre>
</td></tr></table><td></tr></table>


<h2 id="linebyline">Line by Line</h2>

There isn't much non-aKtion specific stuff to show in this step.  The
two major chunks of code are in the constructor and in the
<tt>openFile</tt> method.

<pre><b>
// create our animation widget
widget = new KXAnim(canvas);
widget-&gt;setLoop(true);
widget-&gt;show();
</b></pre>

This creates our animation widget and sets some default parameters.

<PRE><B>
widget-&gt;setFile(m_file);
widget-&gt;stop();
widget-&gt;show();
QTimer::singleShot(2000, this, SLOT(slotPlay()));
</B></PRE>

<p>
And this actually plays the file.  Some hackery needed to be done to
get it to play due to some timing problems with KXAnim.

And that's pretty much it!

</p>

<h2 id="visible">Visible Result</h2>

<img src="img/step3.png" alt="Step 3" width="341" height="244" />

<h2 id="practical">Practical Matters</h2>
<p>
As said earlier, this is one of the shorted and simplest steps in this
example.. but could easily be the hardest in your own application.  It
all depends on how much functionality is encapsulated in your "main"
view object.
</p>
<p>
The key to this step is to have one view object already in your
application in which all processing of the file takes place.  If you
have several classes that all depend on each other with no central
"viewing" object, then you will have problems.
</p>
<p>
As a case study, KGhostView was one of the hardest programs to port to
the KParts system.  The reason for this was that all of the UI
elements were scattered all over the application.  There was no one
coherent interface to the UI aspects -- essentially no way for a third
party app to "view" the same thing that KGhostView was.
</p>
<p>
The solution to this was to create a view class that combined most of
the required UI elements in one place.  After this was done, it was
trivial to plug this view into the KParts system.
</p>

<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#ffcccc">
<b>Things To Remember:</b><br />
The ease of integrating your application into the KParts
architecture depends almost entirely on how encapsulated your UI
elements are into one coherent interface.  The less coherent, the more
problems you will have.
</td></tr></table>
</td></tr></table>
</center>

<?php
  include("footer.inc");
?>
