<?php
  $site_root = "../";
  $page_title = "Embedded Components Tutorial - Page 4";
  include("konqueror.inc");
  include("header.inc");
?>

<div id="quicklinks">
[ Page 1 | <a href="page2.php">2</a> | <a href="page3.php">3</a> | <a href="page4.php">4</a> | <a href="page5.php">5</a> | <a href="page6.php">6</a> | <a href="page7.php">7</a> ]
</div>

<h2>Add Boilerplate KParts Code</h2>

<h2 id="plan">The Plan</h2>
<p>
In this step, you will convert aKtion into an embedded component using
"standard" boilerplate code.  After this step, when you click on an
animation file, it will embed aKtion instead of spawning it off in a
separate process.  Since no aKtion specific code is used in this step,
it won't actually play the animation -- that's reserved for
<a href="page3.php">step 3</a>.
</p>
<p>
The code in this step is completely generic.  You can drop it into any
application with no modifications!
</p>
<p>
You will do this in three baby steps:
</p>
<ol>
<li>Add the aktion_part.cpp and aktion_part.h files</li>
<li>Modify the Makefile.am to reflect the new files</li>
<li>Modify the aktion.desktop file to indicate that it is an embedded
component</li>
</ol>


<h2 id="files">Add KParts Files</h2>
<p>
Add the following two files to your project with no modifications.
</p>
<b><u>aktion_part.h</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#c0c0c0">
<pre>
#ifndef __aktion_part_h__
#define __aktion_part_h__

#include &lt;kparts/browserextension.h&gt;
#include &lt;klibloader.h&gt;

class KInstance;
class AktionBrowserExtension;
<b>class QLabel;</b>

class AktionFactory : public KLibFactory
{
    Q_OBJECT
public:
    AktionFactory();
    virtual ~AktionFactory();

    virtual QObject* create(QObject* parent = 0, const char* name = 0,
                            const char* classname = "QObject",
                            const QStringList &amp;args = QStringList());

    static KInstance *instance();

private:
    static KInstance *s_instance;
};

class AktionPart: public KParts::ReadOnlyPart
{
    Q_OBJECT
public:
    AktionPart(QWidget *parent, const char *name);
    virtual ~AktionPart();

protected:
    virtual bool openFile();

private:
    <b>QLabel *widget;</b>
    AktionBrowserExtension *m_extension;
};

class AktionBrowserExtension : public KParts::BrowserExtension
{
    Q_OBJECT
    friend class AktionPart;
public:
    AktionBrowserExtension(AktionPart *parent);
    virtual ~AktionBrowserExtension();
};

#endif
</pre>
</td></tr></table></td></tr></table>

<b><u>aktion_part.cpp</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#c0c0c0">
<pre>
#include "aktion_part.h"

#include &lt;kinstance.h&gt;
#include &lt;klocale.h&gt;
#include &lt;kaboutdata.h&gt;

#include &lt;qlabel.h&gt;

extern "C"
{
    /**
     * This function is the 'main' function of this part.  It takes
     * the form 'void *init_lib&lt;library name&gt;()  It always returns a
     * new factory object
     */
    void *init_libaktion()
    {
        return new AktionFactory;
    }
};

/**
 * We need one static instance of the factory for our C 'main'
 * function
 */
KInstance *AktionFactory::s_instance = 0L;

AktionFactory::AktionFactory()
{
}

AktionFactory::~AktionFactory()
{
    if (s_instance)
        delete s_instance;

    s_instance = 0;
}

QObject *AktionFactory::create(QObject *parent, const char *name, const char*,
                               const QStringList&amp; )
{
    QObject *obj = new AktionPart((QWidget*)parent, name);
    emit objectCreated(obj);
    return obj;
}

KInstance *AktionFactory::instance()
{
    if ( !s_instance )
    {
        KAboutData about("aktion", I18N_NOOP("aKtion"), "1.99");
        s_instance = new KInstance(&amp;about);
    }
    return s_instance;
}

AktionPart::AktionPart(QWidget *parent, const char *name)
    : KParts::ReadOnlyPart(parent, name)
{
    setInstance(AktionFactory::instance());

    // create a canvas to insert our widget
    QWidget *canvas = new QWidget(parent);
    canvas-&gt;setFocusPolicy(QWidget::ClickFocus);
    setWidget(canvas);

    m_extension = new AktionBrowserExtension(this);

<b>    // as an example, display a blank white widget
    widget = new QLabel(canvas);
    widget-&gt;setText("aKtion!");
    widget-&gt;setAutoResize(true);
    widget-&gt;show();</b>
}

AktionPart::~AktionPart()
{
    closeURL();
}

bool AktionPart::openFile()
{
    widget-&gt;setText(m_file);

    return true;
}

bool AktionPart::closeURL()
{
    return true;
}

AktionBrowserExtension::AktionBrowserExtension(AktionPart *parent)
    : KParts::BrowserExtension(parent, "AktionBrowserExtension")
{
}

AktionBrowserExtension::~AktionBrowserExtension()
{
}
</pre>
</td></tr></table></td></tr></table>


<h2 id="linebylinefactory">Line By Line: Factory</h2>
<p>
All KParts components need to construct two classes: A factory
class derived from KLibFactory and a view class derived from
KParts::ReadOnlyPart.  In this tutorial, they are:
</p>
<pre><b>
#include &lt;kparts/browserextension.h&gt;
#include &lt;klibloader.h&gt;
class AktionFactory : public KLibFactory
class AktionPart : public KParts::ReadOnlyPart
</b></pre>
<p>
The factory object is responsible for instantiating the components and
returning a pointer to them.  This is how Konqueror gets a reference
to your component.
</p>
<p>
The entire process start here:
</p>
<pre><b>
    void *init_libaktion()
    {
        return new AktionFactory;
    }
</b></pre>
<p>
This is the first function that is called in the loading process.  The
form of the name is <b>void *init_<em>libyourapp</em>()</b>  Keep the
part after init_ in mind -- that is the string you will use in the
X-KDE-Library entry <a href="#enable">later on</a>.  This function
always returns a new instance of your factory object
</p>
<p>
You also need to overload two functions in the factory class:
<b>create()</b> and <b>instance()</b>.
</p>
<p>
<pre><b>
QObject *AktionFactory::create(QObject *parent, const char *name, const char*,
                               const QStringList& )
{
    QObject *obj = new AktionPart((QWidget*)parent, name);
    emit objectCreated(obj);
    return obj;
}
</b></pre>
<p>
The <tt>create()</tt> function is called each time your component is
needed.  It is responsible for instantiating a new view object and
returning it.
</p>
<pre><b>
KInstance *AktionFactory::instance()
{
    if ( !s_instance )
    {
        KAboutData about("aktion", I18N_NOOP("aKtion"), "1.99");
        s_instance = new KInstance(&amp;about);
    }
    return s_instance;
}
</b></pre>
<p>
The <tt>instance()</tt> function returns an instance of type
KInstance.  The above code is very standard.  Just use it.
</p>
<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border=0 align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#ffcccc">
<b>Things To Remember:</b><br />
From a practical point of view, all of the factory code is cut and
paste.  Change the references to aktion and Aktion to your own naming
convention and you are set (e.g., <tt>s/aktion/yourapp</tt>,
<tt>s/Aktion/YourApp</tt>)
</td></tr></table>
</td></tr></table>
</center>


<h2 id="linebylineview">Line By Line: View</h2>
<p>
There are six methods that you need to overload for your view class...
but a <em>lot</em> of the code is very cut and paste.  The code in
bold (related to QLabel) is the <em>only</em> application specific
code in the view class!  Here is what each function is doing:
</p>
<pre><b>
AktionPart::AktionPart(QWidget *parent, const char *name)
    : KParts::ReadOnlyPart(parent, name)
{
    setInstance(AktionFactory::instance());

    // create a canvas to insert our widget
    QWidget *canvas = new QWidget(parent);
    canvas-&gt;setFocusPolicy(QWidget::ClickFocus);
    setWidget(canvas);

    m_extension = new AktionBrowserExtension(this);

    // as an example, display a blank white widget
    widget = new QLabel(this);
    widget-&gt;setText("aKtion!");
    widget-&gt;setAutoResize(true);
    widget-&gt;show();
}
</b></pre>
<p>
The constructor is responsible for initializing your internal
variables as well as your "workhorse" object.  Typically, you do not
do much processing inside of your KParts derived class.  Rather,
you have an existing class that has all of the functionality and you
just use that inside of your view class.  In this example, a QLabel
label is used just so there is <em>something</em> displayed when you
run it.
</p>
<p>
Notice that a blank "canvas" is the main widget.  This is because
Konqueror will resize the component to the full width of the view.  In
many cases, you will want to control the size of your component;
both the QLabel and the actual aKtion component are examples of this.
In this case, you set a blank widget as the resized widget and just
use that as the parent of your "real" class.
</p>
<pre><b>
bool AktionPart::openFile()
{
    widget-&gt;setText(m_file);

    return true;
}
</b></pre>
<p>
The <tt>openFile</tt> method is arguably the most important method in
this class.  It is what is called when Konqueror wants your
application to display a file.  There is a lot going on behind the
scenes of this function, though.  The KParts framework itself handles
all file downloads.  That means that by the time <tt>openFile</tt> is
called, the file to play is already local.
</p>
<p>
Note that if your application can handle remote URLs already, then you
can overload the <tt>openURL</tt> function instead of
<tt>openFile</tt>
</p>
<pre><b>
    widget-&gt;setText(m_file);
</b></pre>
<p>
The <tt>m_file</tt> variable is set by the KParts framework.  It is a
QString with a path (not a URL) to a local file.  If the original URL
was a local file, then it will be a path to the actual file.  If it
the original URL was remote, then the path will likely be in the /tmp
directory.
</p>
<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#ffcccc">
<b>Things To Remember:</b><br />
Most of the parts code is cut and paste, also.  Just rename the
references to "aktion" and "Aktion" and insert your own widget in the
place of <tt>widget</tt> and you're set.
</td></tr></table>
</td></tr></table>
</center>


<h2 id="build">Building the Component</h2>
<b><u>Makefile.am</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#c0c0c0">
<pre>
# since the "real" aktion is a library, we start constructing the
# stuff for it
bin_PROGRAMS    = aktion
lib_LTLIBRARIES = libaktion.la

libaktion_la_SOURCES = main.cpp aktionConf.cpp capture.cpp  \
                       aktionVm.cpp kxanim.cpp principal.cpp \
                       <b>aktion_part.cpp</b>
libaktion_la_LDFLAGS = $(all_libraries) -version-info 1:0:0 -module
libaktion_la_LIBADD  = $(LIB_KFILE) $(LIBVM) <b>-lkparts</b>

aktion_SOURCES = main.cpp
aktion_LDADD   = libaktion.la

# these are the headers for your project
noinst_HEADERS = principal.h aktionConf.h kxanim.h capture.h aktionVm.h \
                 <b>aktion_part.h</b>
</pre>
</td></tr></table></td></tr></table>
<p>
The two parts files that you just added to the project need to be added
to the Makefile just like any other file.. and are done so above. You
also need to link to the KParts library since we make extensive use of
it now.
</p>
<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#ffcccc">
<b>Things To Remember:</b><br />
To build an embedded component, you need only to add:<br />
1. Your "parts" files to _SOURCES and _HEADERS<br />
2. The <b>-lkparts</b> library to _LIBADD
</td></tr></table>
</td></tr></table>
</center>


<h2 id="enable">Letting "The System" Know of the Component</h2>
<b><u>aktion.desktop</u></b>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#c0c0c0">
<pre>
[Desktop Entry]
Name=aKtion!
Exec=aktion %i %m -caption "%c"
Icon=aktion
<b>MimeType=video/mpeg;video/x-msvideo;video/quicktime;video/x-flic;</b>
Type=Application
DocPath=aktion/aktion.html
Comment=Video Player
Comment[es]=Reproductor de videos
Terminal=0
<b>ServiceTypes=Browser/View
X-KDE-Library=libaktion</b>
</pre>
</td></tr></table></td></tr></table>
<p>
So far, you've made all the changes necessary to make aKtion an
embedded component (albeit one that doesn't do much).. but there is no
way (yet) for Konqueror to know that when it clicks on a Quicktime
file, it should embed the component instead of spawning an external
viewer.
</p>
<p>
This is where the aktion.desktop file comes in to play.  When you
install the desktop file, the KDE mimetypes system becomes aware of
all of its fields (if it doesn't do it immediately, you can force a
reload by running the <tt>kbuildsycoca</tt> command).  The three that
matter in this case or the <b>MimeType</b>, <b>ServiceTypes</b>, and
<b>X-KDE-Library</b> entries.</p>

<pre><b>
MimeType=video/mpeg;video/x-msvideo;video/quicktime;video/x-flic;
ServiceTypes=Browser/View
X-KDE-Library=libaktion
</b></pre>
<p>
These lines tell the mimetypes system that aKtion can handle the
mimetypes specified in MimeType; it implements an embedded component
of type Browser/View; and the name of the component is libaktion.</p>
<p>
When Konqueror is asked to execute a Quicktime file, it asks the
mimetype system for a corresponding app that can handle
<tt>video/quicktime</tt>.  It also asks if the app can handle
"Browser/View" embedding.  When a pointer to aKtion is returned, it
uses X-KDE-Library to tell it that the component it needs to load is
'libaktion'.
</p>
<center>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td bgcolor="#000000">
<table border="0" align="center" cellspacing="2" cellpadding="0" width="100%">
<tr><td bgcolor="#ffcccc">
<b>Things To Remember:</b><br />
To "enable" your embedded component, you need to have a <tt>.desktop</tt>
file with:<br />
1. A <b>MimeType</b> entry with your supported mimetypes<br />
2. A <b>ServiceTypes=Browser/View</b> entry (exactly like that)<br />
3. A <b>X-KDE-Library=lib<em>yourapp</em></b> entry
</td></tr></table>
</td></tr></table>
</center>

<h2 id="visible">Visible Result</h2>
<img src="img/step2.png" alt="Step 2" width="367" height="166" />

<h2 id="practical">Practical Matters</h2>
<p>
If your own application uses a standard automake like the one above,
then converting it to a shared library uses exactly the same steps
(and code).  There is little specific to aKtion in this procedure.
</p>

<?php
  include("footer.inc");
?>
