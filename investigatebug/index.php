<?php
  $site_root = "../";
  $page_title = "Investigating Konqueror Bugs";
  include("konqueror.inc");
  include("header.inc");
?>

<p>Here is some information about how to investigate a Konqueror bug, to
gather as much useful information as possible before reporting the bug. 
Doing this helps the developers a great deal, because they directly know 
where the problem is, without having to do all the investigation themselves.</p>

<p><strong>Due to the large number of bug reports received and to the large 
amount of time that it takes developers to identify issues, bug reports 
which do not include a reduced test case will get a low priority 
of fixing or may not be confirmed. Reports which are not described in 
reproduceable steps may not be confirmed.</strong></p>

<p>If you are reporting a bug report on Konqueror, then it is 
objectively in your best interests to follow the guidelines
and recommendations listed in this document.</p>

<p>This page is about bugs in the web browser only. For bugs in the
file manager, or about any other KDE application, choose the product
in the <a href="https://bugs.kde.org/enter_bug.cgi?format=guided">guided 
format of the KDE Bug Tracking System</a>.</p>

<h2 id="general-considerations">General considerations when creating a 
bug report on Konqueror</h2>

<p><strong>Creating a bug report is just a first step</strong>
of a long process which may or may not end with a bug fix. The 
Quality Assurance bug triaging people first need to gather all 
the information relevant to the issue or problem, then reproduce 
the issue or problem and then search for possible duplicates 
in the bug tracking system. These steps take considerable 
amount of time to volunteers doing bug triaging.</p>    

<p><strong>Always test with the latest stable available version</strong>.
Otherwise, you may be reporting a bug that has already been fixed 
in the newest stable version of Konqueror. The newest stable 
version of KDE is regularly announced in the  
<a href="http://www.kde.org/#announcements">KDE.org announcements section</a>; 
links to binary packages from your distribution are also often provided. 
Example given: <a href="http://www.kde.org/announcements/4.8/platform.php">KDE 
4.8.0 version release page</a> has a 
<a href="http://www.kde.org/info/4.8.0.php#binary">section listing binary 
packages for KDE 4.8.0</a> for the Gentoo, Kubuntu and openSuse 
distributions. Ideally, you should be 
<a href="http://www.konqueror.org/download/">setting up your software 
packages manager to download and install Konqueror</a>.</p>

<!--
** The Konqueror FAQ is clearly outdated ** 
<p>Also, always give a quick look at the <a href="../faq/">Konqueror FAQ</a>
to see if the bug is a well-known bug, possibly with a known fix or workaround.</p>
** The Konqueror FAQ is clearly outdated **
-->    

<p id="factual"><strong>Be factual as much as possible and remain formal 
when describing the bug.</strong> Rants, advocacy, negativity, 
anger and impatience are not helpful to anyone and will not 
help fix your bug: in fact, these can only make developers avoid 
working on the bug.</p>

<p id="clear-useful"><strong>Be clear and try to bring useful, helpful 
information to the developers when describing, explaining the bug.</strong> 
Bug reporters should always try to bring useful, meaningful information 
into their bug report. Example given: if Konqueror crashes when 
visiting a particular page and you suspect that such crash is 
related to Flash player, then the most useful information you can 
bring in the bug report is to identify the domain name part of the
webpage address (URL) into the bug summary and to state that you 
are using the Flash player from Adobe and to indicate 
<a href="http://www.adobe.com/software/flash/about/">what 
is the Flash player plugin version you are using</a> in the bug 
description. Ideally, the bug summary should state such information. 
Bad is: "Bug summary: Konqueror crashes" is not useful, helpful 
information. In fact, it duplicates the information already contained
in the bug form. Good is: "Crash at www.example.org: Flash player 
related"</p>

<p id="bug-summary"><strong>The single most important piece of information 
of a bug report is the bug summary</strong> A good bug summary will 
identify the issue, will help find such bug report when checking for 
possible duplicates. Searching for possible duplicated bug reports is 
time-consuming: help everyone (Quality Assurance Bug triaging 
people and KDE developers) by editing a meaningful bug summary.
Bad is: "Konqueror crashes". Better is: "Crash at 
www.example.org: Flash player related".</p>

<p>Additional reading: 
<a href="http://dbaron.org/log/20100426-bug-summary">The most 
important field in a bug report: the summary</a></p>

<p id="one-issue-only"><strong>Only one issue per bug report.</strong> 
If a single webpage has several issues or problems or suspected bugs, 
then you should create separate bug reports for each issues that you 
see. This helps isolate issues, reduce complexity and developers' 
time and efforts.</p>

<p>The first step is finding out the type of bug this is about.
The second step is to follow the instructions for this type of bug
(follow the links relating to the type of problem).
And the third step is to find out the right package to report the bug to.</p>

<h2 id="page-empty">Page appears completely empty</h2>

<p>This could be a problem at many levels (HTML, HTTP, Javascript...).</p>

<p>First check that you have Javascript activated:<br />
Settings/Configure Konqueror.../Java &amp; Javascript section/Javascript 
tab/Enable Javascript globally checkbox is checked.<br />Make sure that 
Konqueror allows redirections:<br />
Settings/Configure Konqueror.../Web Browsing/Miscellaneous/Allow automatic 
delayed reloading/redirection;<br />
some sites use Javascript to do redirections etc.</p>

<p>Use <code>wget</code> to download the page, and see if konqueror can
view the local page. If yes, then it's an <a href="#http">HTTP problem</a>.
Also try changing the user agent (in "Configure Konqueror"), to see if the site
isn't giving different results depending on the browser.</p>

<p>If you can't use wget (https site, etc.), try to view the HTML source
from konqueror itself:
<br /> View/View Document Source <kbd>Ctrl+U</kbd>.<br />
If it's empty, then it's an <a href="#http">HTTP problem</a>,
if not, then it's an <a href="#html">HTML/CSS problem</a> or 
a <a href="#js">Javascript problem</a>.</p>

<h2>Page appears as plain text (I see the HTML source)</h2>

<p>This is very probably an HTTP problem, see the <a href="#http">HTTP bug</a>
section.</p>

<h2>Authentication doesn't work, or isn't remembered</h2>
This is very probably an HTTP problem, see the <a href="#http">HTTP bug</a> section.

<h2>Rendering/layout bugs</h2>

<p>If something doesn't appear as it should (not the right position
in the page, not the right font, etc.), see the <a href="#html">HTML/CSS problem</a>
section.</p>

<h2>Some (Javascript) animation or menu doesn't work</h2>

<p>After checking that you have activated Javascript (in "Configure Konqueror"),
go to the <a href="#js">Javascript problem</a> section.</p>

<h2 id="crash">Konqueror crashes</h2>

<p>The most useful information you can provide in that case is a
backtrace, but you need to compile KDE yourself (preferably with
<code>--enable-debug</code>) so the backtrace is useful, or to use
development packages. Official binary packages are "stripped", so
they don't contain any useful information for a backtrace. Simply
paste the backtrace from DrKonqi, into the bugreport.</p>

<p>Very important: a backtrace is good, but it's <em>not enough</em>. 
You must also <strong>indicate the webpage address (URL) where 
the crash occured</strong> and which steps one should follow to 
reproduce the crash.</p>

<h2 id="https">HTTPS sites don't work</h2>

<p>First try other HTTPS sites. If the problem happens only on one,
then try activating different ciphers in the SSL control module, some
sites don't work well with SSLv3, some need TLS, etc. If the problem
happens on all the https sites you try, then it could be an
installation problem.</p>

<p>A common case of problems (such as "the protocol https died
unexpectedly") is if you don't have openssl at all (install it!) or
when your binary packages were built with a certain version of
openssl (for instance 0.9.5a) and you are using openssl 0.9.6. Those
two versions are binary incompatible, so you have to use the same
version as the one your packager used.</p>

<p>If this still doesn't help, follow the instructions for
<a href="#http">HTTP-related problems</a>, to get some debug output
out of kio_http before reporting the bug.</p>

<h2>Anything else</h2>

<p>If the tips on this page don't seem related to your problem,
jump to the next step, <a href="#package">finding the right package</a>.</p>

<hr />

<h2 id="html">HTML/CSS problem</h2>

<p>Your bug is related to HTML and/or CSS. You need to know a bit
HTML and follow these steps to build a <strong id="reduced">reduced 
test case</strong>:</p>

<ol>
<li>Save the relevant page or frame on your disk.</li>
<li>Load the page from your disk and see if the problem is still there.</li>
<li><em>Only</em> if it's necessary to reproduce the bug: save framesets, 
images etc.</li>
<li>Throw out everything that has nothing to do with the bug, i.e. delete some
stuff, reload to check if the problem is still there, delete more stuff.
Do this until you found the <em>exact</em> cause of the problem.
The HTML file should not have more than 20 lines at the end.</li>
<li>The important part is: <strong>the test page should be as short as 
possible</strong> to be able to still reproduce the bug. The shorter it 
is, the sooner it can get fixed!</li>
<li>The HTML code should be properly structured and using valid markup code:
use <a href="http://validator.w3.org/file-upload.html">validator.w3.org</a> 
(note that you can <em>upload</em> files there, they don't have to be online)
to check the basic structure of your document.
Here's an example of a minimal test case that's valid HTML:<br /><br />

<pre>
&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"&gt;

&lt;html&gt;

 &lt;head&gt;

  &lt;title&gt;Reduced test case for [Bug number and bug summary]&lt;/title&gt;

 &lt;/head&gt;

 &lt;body&gt;

  &lt;p&gt;insert the problematic HTML here&lt;/p&gt;

 &lt;/body&gt;

&lt;/html&gt;
</pre>
<br /></li>

<!--
<li>If the problem appears only with an invalid page, but not with a valid
one, first mail the webmaster of the page. The bug is in his page, not
in our browser. You may still submit this as a bug, but please
make clear in your report that it only happens with invalid pages.</li>
-->

<li>Submit a bug report to <a href="http://bugs.kde.org/">the KDE bug system</a>
with the test case in the bug report, and the description of the problem.
You can add "testcase" into the keywords list of the bug so that
it's easier for the developers to find bug reports with test cases.</li>
</ol>

<p>Creating a reduced test case is universally important for all browser
vendors and several software manufacturers which have bug tracking system. 
If you are the web author having a web site and you are reporting a bug 
affecting one of your webpage, then you are the best person in a position
to create a reduced test case.</p>

<p>Additional reading:</p>

<ul>
  <li><a href="http://www.webkit.org/quality/reduction.html">Test Case 
  Reduction</a></li>
  <li><a href="https://developer.mozilla.org/en/Reducing_testcases">Reducing 
  testcases</a></li>
</ul>

<h3 id="rendering-mode">Identify Konqueror rendering engine and 
webpage rendering mode</h3>

<p>In the bug description, you can <strong>identify the rendering 
engine</strong> (KHTML or WebKit) that your Konqueror software is using. 
Do:<br />
<strong>Settings/Configure Konqueror.../General section/Default web 
browser engine</strong></p>

<p>Webpages can be rendered in 3 different rendering modes: Strict, 
Almost standards and Quirks. The best and most web interoperable rendering 
mode for all browsers is Strict (usually called web standards 
compliant rendering mode). In the bug description, you can <strong>identify 
the rendering mode used</strong> to display the webpage on which you are 
reporting a bug. Do:<br />
<strong>View/View Document Information <kbd>Ctrl+I</kbd>/ Rendering 
mode:</strong></p>

<h2 id="http">HTTP or FTP problem</h2>

<p>Here's how to debug an HTTP or FTP problem,
if you have compiled KDE from sources, or on distributions which do
not disable debug output completely.</p>

<p>The goal here is to generate and access debug output messages,
so first we have to make sure where it will end up.</p>

<ul>
<li>If you start X using "startx", and if your distribution doesn't
automatically redirect output messages into a file in your
home directory, then you should restart X using
<code>startx 2&gt;&amp;1 | tee ~/xsession.log</code>, and the debug
output will go into ~/xsession.log.</li>
<li>If you start X using login manager such as kdm, xdm, or gdm,
then the debug output automatically goes into ~/.xsession-errors
on most distributions (RedHat, Mandriva, Debian, Kubuntu...) and
goes into ~/.X.err on SuSE.</li>
</ul>

<p>Now we can generate the debug output:</p>

<ul>
 <li>Run <code>kdebugdialog</code>.</li>
 <li>Make sure the checkbox "Disable all debug output" is unchecked (KDE &gt;= 4.4).</li>
 <li>HTTP: type 'http' and check all the checkboxes (in particular 7103 and 7113).</li>
 <li>FTP: type 'ftp' and check all checkboxes.</li>
 <li>Press Ok.</li>
 <li>Restart KDE (or simply kill any kio_http or kio_ftp process).</li>
 <li>Open konqueror onto the webpage you're interested in.</li>
 <li>Look for the debug output at the location previously determined.</li>
</ul>

<p>Now, actually digging info out of the debug output requires to know the
protocol. If you don't, try to reduce the debug output to what starts
with kio_http/kio_ftp, and send it as part of the bug report.</p>

<h2 id="js">Javascript problem</h2>

<p>A very useful piece of information (besides a reduced test case that
shows the bug) is the debug output from konqueror, since any javascript
error is printed there (assuming you're not using RPMs with disabled
debug output).</p>

<p>Simply start "konqueror" from a terminal, and open the web page in question.
The lines startings with "JS:" are those printed by the javascript interpreter.
If you see any errors, report them as part of the bug report.</p>

<p id="js-debugger"><strong>For experienced users</strong>: alternatively, 
you can enable the reporting of javascript errors with<br />
Settings/Configure Konqueror.../Java &amp; Javascript section/Javascript 
tab/Debugging section/Enable debugger and Report Errors checkboxes checked</p>

<hr />

<p id="package">Once you have gathered the required information,
find the right package to which the bug should be reported to:</p>

<table border="1">
<tr>
<th>Package</th>
<th>Problem</th>
</tr>
<tr>
<td>konqueror</td>
<td>Any bug that is not inside the "view", but part of the
framework, e.g. the toolbar</td>
</tr>
<tr>
<td>khtml</td>
<td>Rendering/layout bugs, crashes that don't go away when you visit a html
page and have Javascript and Java disabled</td>
</tr>
<tr>
<td>kjs</td>
<td>Any dynamic Javascript thingie not working or crashing. Might be a bug in
khtml, but as it's difficult to judge for the user, this package line is fine</td>
</tr>
<tr>
<td>kjava</td>
<td>Java related problems</td>
</tr>
<tr>
<td>kssl</td>
<td>https (SSL) related problems</td>
</tr>
<tr>
<td>kio_http</td>
<td>Problems with the http communication itself.</td>
</tr>
<tr>
<td>kcookiejar</td>
<td>Cookie related problems</td>
</tr>
</table>

<p>Now go to <a href="http://bugs.kde.org/">the KDE bug system</a>
to send in a bug report. Thanks!</p>

<p>Additional reading:</p>

<ul>
  <li><a href="http://blogs.fsfe.org/myriam/2011/10/when-is-a-bug-report-useful/">When is a bug report useful?</a> Myriam's blog, October 20th 2011</li>    
  <li><a href="http://userbase.kde.org/Asking_Questions#Reporting_KDE_Bugs">Reporting KDE Bugs</a> KDE UserBase Wiki</li>     
</ul> 
    
<?php
  include("footer.inc");
?>
