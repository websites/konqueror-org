<?php
  $site_root = "../";
  $page_title = "Download Konqueror";
  include("konqueror.inc");
  include("header.inc");
?>

<p>Please follow the steps for your distribution on how to install Konqueror.</p>
<p><strong>Be warned that if you don't have KDE SC installed you will be prompted to install many of the KDE libraries as Konqueror depends on them to run successfully</a>.</strong></p>

<h2>Ubuntu/Debian</h2>
<code># apt-get install konqueror</code>

<h2>Fedora/Red Hat</h2>
<code># dnf install konqueror</code>

<h2>openSUSE</h2>
<code># zypper install konqueror</code>

<h2>Arch Linux</h2>
<code># pacman -S konqueror</code>

<h2>Windows and Mac OS</h2>
<p>Please follow the instructions on <a href="http://www.kde.org/download/">how to install KDE</a> on Windows and Mac OS.</p>

<?php
  include("footer.inc");
?>
