<?php
  $site_root = "../";
  $page_title = "Konqueror FAQ (Outdated)";
  include("konqueror.inc");
  include("header.inc");
?>

<?php

$faq = new FAQ("Konqueror FAQ");

$faq->addQuestion("Konqueror refuses to login into netscape mail. Is there a fix for that?",
"By default konqueror does not accept cookies that originate from a site other than the one you
are currently visiting. This is done to protect your privacy. Unfortunately, the netscape mail
site requires that you accept cookies that come from sites other than their own. Hence, the
only way to make konqueror work with that site is to disable the option that activates the
feature mentioned above. To disable this option do the following: </p>
<ul>
<li>In konqueror click on the <b>Settings</b> menu.</li>
<li>Select <b>Configure Konqueror...</b></li>
<li>In the <i>Configure Konqueror</i> dialog, click on <b>Cookies</b>.</li>
<li>Make sure the option labeled <b>Only accept cookies from originating servers</b>
is unchecked.</li>
</ul>
<p>
");

$faq->addQuestion("Is there a way to change the default Accept-Charset/Accept-Language HTTP headers in konqueror?",
"Yes. By default konqueror automatically uses the settings configured when you installed your
desktop. You can manually override this behavior by adding the following settings to either <code>
\$KDEHOME/share/config/kioslaverc</code> or <code>\$KDEHOME/share/config/kio_httprc</code>:
</p>
<pre>
Languages=&lt;comma-separated-list-of-languages&gt;
Charsets=&lt;comma-separated-list-of-charsets&gt;
</pre>
<p>
If you add the settings to the first \"kioslaverc\", then the options are available to all KDE ioslaves that support
changing these settings. On the other hand, if you want the settings to be applicable to a specific io-slave, e.g.
kio_httprc, then you want to add them to the protocol specific config files. You can also specify these
settings on a per-site basis. To do that, instead of adding the settings to the top of the file (global section),
you need to add them to a specific site section. For example to change the settings sent to all <b>kde.org</b>
sites you would do the following:
</p>
<pre>
[kde.org]
Languages=en
Charsets=iso-8859-1
</pre>
<p>
After making these changes you need to issue the following command to make the settings
take immediate effect:
</p>
<pre>
dcop konqueror-* KIO::Scheduler reparseSlaveConfiguration \"http\";
</pre>
<p>&nbsp;
"); // Last paragraph is a dummy paragraph for XHTML validation

$faq->addQuestion("How do I tell Konqueror to forget all the cached logins and passwords?",
"When you successfully login to a web site (using the login/password dialog, not using a form),
or to an FTP/SMB/SSH/... site, Konqueror caches the username and password in the
kdesud daemon. This information remains alive as long as kdesud runs, but isn't
saved to any file. To ask it to forget all the currently cached logins and passwords,
simply run
<br/>
<code>kdesu -s</code>
");

$faq->addQuestion("Where does Konqueror store its history, and how do I erase it?",
"There are several kinds of \"history\" stored by Konqueror.
</p>
<ul>
<li>The location bar combobox items and completion: you can clear those with right-click, \"Clear History\".</li>
<li>The history module in the sidebar: you can clear it with a right-click on one of the host names, and selecting \"Clear History\".</li>
<li>The HTTP cache (known as \"Temporary Internet Files\" in other environments) can be cleared in the KControl module called \"Cache\", using the \"Clear cache\" button on the bottom right.</li>
</ul>
<p>
File locations (<code>~/.kde</code> is only an indication. Your distributor, or \$KDEHOME, could have modified that value) :
</p>
<ul>
<li>Combobox items: <code>~/.kde/share/config/konq_history</code></li>
<li>History module and combobox completion: <code>~/.kde/share/apps/konqueror/konq_history</code> (binary file)</li>
<li>HTTP cache: <code>~/.kde/share/cache/http/</code> (previously in <code>~/.kde/share/apps/kio_http/cache/</code>)</li>
</ul>
<p>&nbsp;
"); // Last paragraph is a dummy paragraph for XHTML validation

$faq->addQuestion("Why does Konqueror crash on every page with Flash? (old)",
"This could be a result of a clashing symbol in both the flash plugin and the
XFree86 libGLU (OpenGL utility lib). Upon closing an embedded flash view,
the wrong function is called which heavily corrupts memory and leads to
either immediately or delayed crashes, lockups and worse.
<br />
The only solution that is currently known is either to install Qt without
OpenGL support or to not use the Flash plugin. You can't combine both
until this symbol clash is somehow solved. Unfortunately we cannot do
much about this issue, unless Macromedia is willing to help.
<br />
Another reason for Konqueror to crash on every page using a Netscape plugin
is the use of gcc3. Plugins can't work with gcc3 because they are linked to
gcc2's libstdc++, which is incompatible with gcc3's libstdc++.
");

$faq->addQuestion("Does Konqueror support automatic ftp logins via \".netrc\" files",
"Ever since version 2.2, konqueror does indeed support automatic
logins as specified in the \".netrc\" file.  However, there is no graphical
front end to enable this feature ; so you have to do it manually using the
following procedure:
</p>
<ol>
<li>Create <tt>\$KDEHOME/share/config/kio_ftprc</tt> if it does not already exist.
<br /><code>Note that \$KDEHOME referes to your personal kde config directory (usually
~/.kde).</code></li>
<li>Add the following entry to kio_ftprc: \"EnableAutoLogin=true\"</li>
<li>Issue the following command to update running io-slaves:<br />
<code>dcop konqueror-* KIO::Scheduler reparseConfiguration \"\"</code>
</li>
</ol>
<p>
Please note that the support for the \"macro\" keyword is very limited in the current
implementation.  In fact, we only honor the \"cd\" command.  Furthermore, eventhough
this nominal \"macro\" support was left ON by default for the 2.2 beta1 release, you
will have to be manually enable it in subsequent releases by adding a \"EnableAutoLoginMacro=\"
entry following the same procedure outlined above.
");

$faq->addQuestion("I'm using an NVidia driver for X, and Konqueror hangs the system (old)",
"Add</p>
<pre>Option \"NvAgp\" \"2\"</pre>
<p>to the \"Device\" section for the NVidia card, in XF86Config.
The above option just says to use the Linux agpgart module instead of Nvidia's own.
Thanks to Scott Lanham for this advice.
");

$faq->addQuestion("My wheel mouse sometimes does not work with Konqueror",
"Disable imwheel, at least for Konqueror (you can add this to the Exclude section in ~/.imwheelrc).
You might consider not using imwheel at all, because it causes more problems than they solve
with applications these days.
");

$faq->addQuestion("Where does the name <i>Konqueror</i> come from?",
"It's a word play on the other browsers' names. After the Navigator and the
Explorer comes the Conqueror; it's spelled with a K to show that it's part of
KDE. The name change also moves away from \"kfm\" (the KDE file manager,
Konqueror's predecessor) which represented only file management.
");

$faq->addQuestion("Why doesn't KHTML show the contents of an image's ALT tag in a tooltip?",
"There's no standard that says an ALT tag should appear when you
hover over an image. Some browsers do so, but that's all.<br/>
Most of the time the ALT tag contains some useless description
like \"foo.jpg (231432 bytes)\" as Frontpage does by default; besides, the
specification for the ALT property calls for ALT to be displayed in place of
the image, as in text-mode-only browsers such as lynx or w3m. It is not meant
to be used as a tooltip. Tooltips for images are supposed to come from the TITLE attribute,
and this is being implemented.
");

$faq->addQuestion("What is Konqueror's User Agent string?",
"There is no definite answer to this question.
<br />
The <code>HTTP_USER_AGENT</code> http header and the properties of the
window.navigator JavaScript object have been subject to change during
the last KDE releases. Additionally, the user can modify the UA
information on a domain-by-domain basis pretending to have any other
browser in existence.
<br />
If you are reading this because you wanted to uniquely identify
Konqueror on your website see the answer to the next question.
");

$faq->addQuestion("As an author of a web site, how can I identify Konqueror from other
browsers?",
"We are working very hard on making all the differences go away thus
making any differentiation impossible - or rather needless.
<br />
In case you experience a bug or missing feature in Konqueror (let's
say the JavaScript function document.foo() is not working as expected)
we recommend to test for features rather than the browsers brand.
<br />
Rather than writing code like</p>
<pre>
  if (navigator.userAgent.indexOf(\"Konqueror\") &gt;= 0)
     IEorMozilla = false;
</pre>
we recommend to perform direct checks such as
<br /><br /><pre>
  var workingFoo = (foo(\"bar\") == 42); // doing what it's supposed to do ?
  var DOM = !!document.getElementById; // basic DOM support ?
  var documentALL = !!document.all; // Microsoft extension available ?
</pre><br />

This way you don't
<ul>
  <li>discriminate against any kind of browser. No matter how exotic
  they are.</li>
  <li>get trapped by browsers that have to disguise their
  identity to slip through incomplete or wrong user agent checks.</li>
  <li>have to maintain any version dependency in your check in case a
  future maintenance release fixes the issue.</li>
</ul>
<p>&nbsp;"); // Last paragraph is a dummy paragraph for XHTML validation

$faq->addQuestion("How do I set my 'home' page - the page loaded on startup?",
"This is now available in \"Configure Konqueror\", page \"General\", field \"Home page\""
);

$faq->addQuestion("Is there a way to change the timeout values in Konqueror?",
"You can change the timeout settings in the system settings under <code>\"Network->Connection Preferences\"</code>.
<br />
If you would rather do it by hand, the configuration file for this is \$KDEHOME/share/config/kioslaverc,
where \$KDEHOME is your local kde config directory (<em>usually ~/.kde/</em>).
</p>
<ol>
</li>
<li>Add the following entries without any HEADING, i.e outside of any [] block.
Simply adding it to the top of the file will suffice.
<br /><br />
<pre>ReadTimeout=x                       // length of time to wait for arrival of requested data
ResponseTimeout=x                   // length of time to wait for a response after sending a request
ConnectTimeout=x                    // length of time to wait for response after attempting to connect
ProxyConnectTimeout=x               // same a above except it is used for proxy servers.
</pre>
<br />
where x is the value you need in seconds.  To give you an idea, here are the default
values:
<br /><br />
<pre>DEFAULT_READ_TIMEOUT                    15     // 15 SECONDS
DEFAULT_RESPONSE_TIMEOUT                60     //  1 MINUTE
DEFAULT_CONNECT_TIMEOUT                 20     // 20 SECONDS
DEFAULT_PROXY_CONNECT_TIMEOUT           10     // 10 SECONDS</pre>
</li>
<li>Manually run the following command to update any running io-slaves:
<br /><br />
<pre>dcop konqueror-* KIO::Scheduler reparseConfiguration \"\"</pre></li>
</ol>
<p>
");

$faq->addQuestion("Can I run Konqueror without running KDE?",
"Yes. Just install Qt, kdelibs and kdebase, and from your favourite
window manager, just launch Konqueror. It should work just fine,
but if it doesn't (KDE developers don't test that case often), report it
to http://bugs.kde.org and try running \"kdeinit4\" before running Konqueror,
it usually helps.
<br />
This is of course the same for any other KDE application.
");

$faq->addQuestion("What is the format of addresses in the \"No proxy for\" dialog box?",
"The \"No Proxy For\" field under \"Settings->Configure->Proxies\" accepts any
number of <b>comma or space</b> separated hostnames or fully qualified
addresses.  You can mix and match the use of these separators as well as
have any number of them b/n the specified hostnames in the list.
</p>
<pre>
Example: 1.) kde.org,,,,,,,  mycompanyintranet.com
         2.) kde.org  mycompanyintranet.com
         3.) kde.org, mycompanyintranet.com
</pre>
<p>
<em>Do not however use the asterisk (*) to indicate multiple matches under
a specific domain.</em> That will not work. Instead only enter the common part of
the addresses you want to exclude.  For example, to exclude all KDE sites from
using proxy enter \".kde.org\" (without the quotation) rather than \"*.kde.org\"
in the \"No Proxy For\" field.
Also note that this is only supported for HTTP proxies currently, not
for FTP proxies.
");

$faq->addQuestion("How can I connect to an FTP/SMB site that requires a login and password?",
"Simply type <tt>ftp://user@host/</tt> and Konqueror will prompt you for a password.
If your username contains the '@' sign, replace it with %40.
If your username contains the '#' sign, replace it with %23.
You can also type the password into the URL, using ftp://user:password@host,
using the same encoding rules for '@' and '#' in the password.
");

$faq->addQuestion("How do I add bookmarks to the bookmark toolbar?",
"Under the Bookmarks menu, select Edit Bookmarks.
This will bring up a new window, showing the contents of the bookmarks directory.
From there, you can copy bookmarks into the <i>Toolbar</i> sub-directory,
and every bookmark you copy there will appear in the bookmark toolbar.
<br />
Using the \"Edit toolbars\" dialog for this won't work, this toolbar works differently.
<br />
");

$faq->addQuestion("How do I enable SSL support?",
"Most binary distributions come with SSL support enabled, but if you compile
from sources, you need to install a recent openssl, remove CMakeCache.txt
and re-run cmake, in kdelibs.
");

$faq->addQuestion("How do I enable support for Netscape plugins?",
"Most binary distributions come from Netscape plugins support enabled, but if you compile
from sources, you need to install libXt (e.g. package libxt-dev), remove CMakeCache.txt
and re-run cmake, in kdebase (check that it compiles the contents of
kdebase/apps/nsplugins/). If the auto-detection of the plugins fails, try
running \"nspluginscan\" from the command line.
");

$faq->addQuestion("Java Applets only show up as grey rectangles, what's wrong?",
"Most like you are using Sun's 1.3 JRE.  There is a know problem that prevents KJAS from
working with that version of Java.  Please use Blackdown's 1.3 release, IBM's 1.3 release,
or Sun's 1.2.2 release.  Each of those is known to work.  Hopefully this issue will be
resolved in a 1.3.1 release from Sun.
");

$faq->show();

?>

<?php
  include("footer.inc");
?>
