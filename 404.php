<?php
include("handler.inc");
$handler = new Handler404();
$handler->add("/bug.html", "/investigatebug");
$handler->add("/c-tutorials.html", "/componentstutorial");
$handler->add("/developers.html", "/developers");
$handler->add("/embedded.html", "/embedded");
$handler->add("/faq.html", "/faq");
$handler->add("/install-binaries.html", "/index.php");
$handler->add("/install-source.html", "http://developer.kde.org/build/compile_kde3_1.html");
$handler->add("/konq-application.html", "/features/application.php");
$handler->add("/konq-browser.html", "/features/browser.php");
$handler->add("/konq-filemanager.html", "/features/filemanager.php");
$handler->add("/konq-java.html", "/javahowto");
$handler->add("/konq-viewer.html", "/features/viewer.php");
$handler->add("/announcements/reaktivate.html", "/announcements/reaktivate.php");
$handler->add("/announcements/konqueror.org.html", "/announcements/sitelaunch.php");
$handler->add("/index.html", "/index.php");
$handler->add("/announcements/index.html", "/announcements");
$handler->add("/content/khtml_css2.html", "/css");
$handler->execute();
?>
