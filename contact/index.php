<?php
  $site_root = "../";
  $page_title = "Contact us";
  include("konqueror.inc");
  include("header.inc");
?>

<h2>IRC</h2>
<p>Join <a href="irc://irc.freenode.org/#khtml">#khtml</a> on Freenode.</p>

<h2>Mailing list</h2>
<p>Subscribe to <a href="https://mail.kde.org/mailman/listinfo/kfm-devel">kfm-devel</a>.</p>

<h2>Report a bug</h2>
<p>Use <a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product=konqueror">KDE's Bug Tracking System</a>.</p>

<?php
  include("footer.inc");
?>
