<?php
$this->setName ("Konqueror Homepage");

$section =& $this->appendSection("Inform");
$section->appendLink("Home","");
$section->appendLink("KDE Home","http://www.kde.org/",false);
$section->appendDir("Announcements", "announcements");
$section->appendLink("News","news.php");
$section->appendDir("Features", "features");
$section->appendLink("Konqueror Embedded", "embedded");

$section =& $this->appendSection("Technical");
$section->appendLink("Konqueror FAQ", "faq");
$section->appendLink("Java HOWTO", "javahowto");
$section->appendLink("CSS Support", "css");

$section =& $this->appendSection("Download");
$section->appendLink("Download Konqueror", "download");

$section =& $this->appendSection("Develop");
$section->appendLink("Developers", "developers");
$section->appendLink("Investigating Bugs", "investigatebug");
$section->appendDir("Embedded Components Tutorial", "componentstutorial");
?>
