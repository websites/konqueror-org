<?php
  $site_root = "../";
  $page_title = "Developers";
  include("konqueror.inc");
  include("header.inc");
?>

<table border="0" cellpadding="4">

 <tr>
  <td>David Faure</td>
  <td>framework, parts, Javascript, I/O lib and maintainer</td>
  <td>faure kde.org</td>
 </tr>

 <tr>
  <td>Simon Hausmann</td>
  <td>framework, parts</td>
  <td>hausmann kde.org</td>
 </tr>

 <tr>
  <td>Michael Reiher</td>
  <td>framework</td>
  <td>michael.reiher gmx.de</td>
 </tr>

 <tr>
  <td>Matthias Welk</td>
  <td>framework</td>
  <td>welk fokus.gmd.de</td>
 </tr>

 <tr>
  <td>Alexander Neundorf</td>
  <td>List views</td>
  <td>neundorf kde.org</td>
 </tr>

 <tr>
  <td>Michael Brade</td>
  <td>List views, I/O lib</td>
  <td>brade kde.org</td>
 </tr>

 <tr>
  <td>Lars Knoll</td>
  <td>HTML rendering engine</td>
  <td>knoll kde.org</td>
 </tr>

 <tr>
  <td>Dirk Mueller</td>
  <td>HTML rendering engine</td>
  <td>mueller kde.org</td>
 </tr>

 <tr>
  <td>Waldo Bastian</td>
  <td>HTML rendering engine, I/O lib</td>
  <td>bastian kde.org</td>
 </tr>

 <tr>
  <td>Germain Garand</td>
  <td>HTML rendering engine</td>
  <td>germain ebooksfrance.org</td>
 </tr>

 <tr>
  <td>Leo Savernik</td>
  <td>HTML rendering engine</td>
  <td>l.savernik aon.at</td>
 </tr>

 <tr>
  <td>Stephan Kulow</td>
  <td>HTML rendering engine, I/O lib, regression test framework</td>
  <td>coolo kde.org</td>
 </tr>

 <tr>
  <td>Antti Koivisto</td>
  <td>HTML rendering engine</td>
  <td>koivisto kde.org</td>
 </tr>

<tr>
<td>Zack Rusin</td>
<td>HTML rendering engine</td>
<td>zack kde.org</td>
</tr>

<tr>
<td>Tobias Anton</td>
<td>HTML rendering engine</td>
<td>anton stud.fbi.fh-darmstadt.de</td>
</tr>

<tr>
<td>Lubos Lunak</td>
<td>HTML rendering engine</td>
<td>l.lunak kde.org</td>
</tr>

<tr>
<td>Allan Sandfeld Jensen</td>
<td>HTML rendering engine</td>
<td>kde carewolf.com</td>
</tr>

 <tr>
  <td>Apple Safari Developers</td>
  <td>HTML rendering engine, Javascript</td>
  <td></td>
 </tr>

 <tr>
  <td>Harri Porten</td>
  <td>Javascript</td>
  <td>porten kde.org</td>
 </tr>

 <tr>
  <td>Koos Vriezen</td>
  <td>Java applets and other embedded objects</td>
  <td>koos.vriezen xs4all.nl</td>
 </tr>

 <tr>
  <td>Matt Koss</td>
  <td>I/O lib</td>
  <td>koss napri.sk</td>
 </tr>

 <tr>
  <td>Alex Zepeda</td>
  <td>I/O lib</td>
  <td>jazepeda pacbell.net</td>
 </tr>

 <tr>
  <td>Peter Kelly</td>
  <td>Javascript, HTML rendering engine</td>
  <td>pmk post.com</td>
 </tr>

 <tr>
  <td>Carsten Pfeiffer</td>
  <td>framework</td>
  <td>pfeiffer kde.org</td>
 </tr>

 <tr>
  <td>Wynn Wilkes</td>
  <td>Java 2 security manager support, and other major improvements to applet support</td>
  <td>wynnw calderasystems.com</td>
 </tr>

 <tr>
  <td>Richard Moore</td>
  <td>Java applet support</td>
  <td>rich kde.org</td>
 </tr>

 <tr>
  <td>Dima Rogozin</td>
  <td>Java applet support</td>
  <td>dima mercury.co.il</td>
 </tr>

 <tr>
  <td>Stefan Schimanski</td>
  <td>Netscape plugin support</td>
  <td>1Stein gmx.de</td>
 </tr>

 <tr>
  <td>George Staikos</td>
  <td>SSL support</td>
  <td>staikos kde.org</td>
 </tr>

 <tr>
  <td>Dawit Alemayehu</td>
  <td>I/O lib, Authentication support</td>
  <td>adawit kde.org</td>
 </tr>

 <tr>
  <td>Torben Weis</td>
  <td>kfm author</td>
  <td>weis kde.org</td>
 </tr>

 <tr>
  <td>Chris Lee</td>
  <td>web site</td>
  <td>webmaster konqueror.org</td>
 </tr>

 <tr>
  <td>Stephan Binner</td>
  <td>Misc bug fixing and improvements</td>
  <td>binner kde.org</td>
 </tr>

</table>

<p>
 Konqueror uses OpenSSL, Copyright &copy; 1995-1998 Eric Young (eay cryptsoft.com),
 see <a href="http://www.openssl.org">www.openssl.org</a>.
</p>

<p>
 For any question about Konqueror, please mail <a href="mailto:kfm-devel@kde.org">
the developer's mailing list, kfm-devel</a> (mention whether you're subscribed to the
list when posting to it, see <a href="http://mail.kde.org/mailman/listinfo/kfm-devel/">here</a> for subscribing)
instead of the individual developers.
</p>

<p>
 There's an archive of the <a href="http://lists.kde.org/?l=kfm-devel&amp;r=1&amp;w=2">developer's
 mailing list</a>.
</p>

<p>
 Note that for bug reports or feature requests, you should rather
 use the <a href="http://bugs.kde.org">bug-tracking system</a>.
</p>

<?php
  include("footer.inc");
?>
